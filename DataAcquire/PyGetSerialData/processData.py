import pandas as pd
from matplotlib import pyplot as plt
import os
import preprocessing as pp
import FileOps as fo
import statistics

Motion = "4" #which Motion should be validated
paths = ["../MotionData/" + Motion + "/RawData"]
path = "../MotionData/" + Motion

preprocess = True

#read raw Motion values
files, lMotionData = fo.getFilesFromFolder(paths)

#preprocessing
gLen = 50
normXYZ = []
normMotions = []
dfListNorm = []
if preprocess:
    for i in range(len(lMotionData)):
        #offset
        X = pp.OffsetCorr(lMotionData[i]['x'])
        Y = pp.OffsetCorr(lMotionData[i]['y'])
        Z = pp.OffsetCorr(lMotionData[i]['z'])
        #plt.subplot(121); plt.plot(lMotionData[i]['z']); plt.grid(True); plt.ylim((-16000, 35000))
        #plt.subplot(122); plt.plot(Z); plt.grid(True); plt.ylim((-16000, 35000))
        #plt.show()
        #interpolate
        deltaLen = (len(X) - 1) / float(gLen - 1)
        XOld = X.copy()
        X = [pp.interpolate(X, i * deltaLen) for i in range(gLen)]
        Y = [pp.interpolate(Y, i * deltaLen) for i in range(gLen)]
        Z = [pp.interpolate(Z, i * deltaLen) for i in range(gLen)]
        #plt.subplot(121); plt.plot(XOld); plt.grid(True); plt.xlim((0, 100))
        #plt.subplot(122); plt.plot(X); plt.grid(True); plt.xlim((0, 100))
        #plt.show()
        #average
        XOld = X.copy()
        X = pp.MovingAverage(X,5)
        Y = pp.MovingAverage(Y,5)
        Z = pp.MovingAverage(Z,5)
        #plt.subplot(121); plt.plot(XOld); plt.grid(True); plt.ylim((-15000, 15000))
        #plt.subplot(122); plt.plot(X); plt.grid(True); plt.ylim((-15000, 15000))
        #plt.show()
        #normalize
        XOld = X.copy()
        X = pp.Normalize(X, 100)
        Y = pp.Normalize(Y, 100)
        Z = pp.Normalize(Z, 100)
        #plt.subplot(121); plt.plot(XOld); plt.grid(True)
        #plt.subplot(122); plt.plot(X); plt.grid(True)
        #plt.show()

        normXYZ.append(X.copy()); X.clear()
        normXYZ.append(Y.copy()); Y.clear()
        normXYZ.append(Z.copy()); Z.clear()
        normMotions.append(normXYZ.copy());normXYZ.clear()

i = 0
while i < len(normMotions):
    if i < len(normMotions):
        plt.subplot(221).set_title(files[i])
        plt.plot(normMotions[i][0], '-', linewidth=1, label="X")
        plt.plot(normMotions[i][2], '-', linewidth=1, label="Z")
        plt.legend(loc="upper left")
    if i+1 < len(normMotions):
        plt.subplot(222).set_title(files[i+1])
        plt.plot(normMotions[i+1][0], '-', linewidth=1, label="X")
        plt.plot(normMotions[i+1][2], '-', linewidth=1, label="Z")
    if i+2 < len(normMotions):
        plt.subplot(223).set_title(files[i+2])
        plt.plot(normMotions[i+2][0], '-', linewidth=1, label="X")
        plt.plot(normMotions[i+2][2], '-', linewidth=1, label="Z")
    if i+3 < len(normMotions):
        plt.subplot(224).set_title(files[i+3])
        plt.plot(normMotions[i+3][0], '-', linewidth=1, label="X")
        plt.plot(normMotions[i+3][2], '-', linewidth=1, label="Z")
    plt.figure();
    i +=4
#plt.show()

# store normed Motions in File
#for i in range(len(normMotions)):
    #fo.DataToFile(path+"/normData", normMotions[i], ['x','y','z'], files[i])

lMeans = []
lAvgs = []
lMeansX = []; lAvgsX = []
lMeansZ = []; lAvgsZ = []
for i in range(len(normMotions[0][0])):
#    for j in range(len(normMotions)):
    for j in range(5):
        lMeans.append(normMotions[j][0][i])
        lAvgs.append(normMotions[j][0][i])
    lMeansX.append(statistics.median(lMeans));lMeans.clear()
    lAvgsX.append(statistics.mean(lAvgs));lAvgs.clear()
#    for j in range(len(normMotions)):
    for j in range(5):
        lMeans.append(normMotions[j][2][i])
        lAvgs.append(normMotions[j][2][i])
    lMeansZ.append(statistics.median(lMeans));lMeans.clear()
    lAvgsZ.append(statistics.mean(lAvgs));lAvgs.clear()
lAvgsX = pp.Normalize(lAvgsX, 100)
lAvgsZ = pp.Normalize(lAvgsZ, 100)
#for i in range(len(normMotions)):
for i in range(5):
    plt.subplot(221)
    plt.plot(normMotions[i][0])
    plt.subplot(222)
    plt.plot(normMotions[i][2])

lAvgsX = pp.Normalize(lAvgsX, 100);# lAvgsX = pp.OffsetCorr(lAvgsX)
lAvgsZ = pp.Normalize(lAvgsZ, 100);# lAvgsZ = pp.OffsetCorr(lAvgsZ)
plt.subplot(223)
plt.plot(lMeansX, color="blue")
plt.plot(lAvgsX, color="orange")
plt.subplot(224)
plt.plot(lMeansZ, color="blue")
plt.plot(lAvgsZ, color="orange")
#plt.show()
#fo.DataToFile(path+"/DTWSeq",[lAvgsX, lAvgsZ], ['x','z'], "DTW_"+Motion+".csv")

idxs = [0,1,2,3,5]
plt.subplot(211)
for i in range(5):
    plt.plot(normMotions[idxs[i]][0])
plt.subplot(212)
plt.plot(lAvgsX)


plt.show()

plt.subplot(121); plt.plot(normMotions[0][0]); plt.grid(True)
xP = pp.CalcPitch(normMotions[0][0], 5)
plt.subplot(122); plt.plot(pp.ClassifyPitch(xP), '.', label="X")
#plt.show()

plt.clf()
xM = normMotions[2][0];  xM = [i / 100 for i in xM]
zM = normMotions[2][2];  zM = [i / 100 for i in zM]
xP = pp.CalcPitch(xM, 5)
zP = pp.CalcPitch(zM, 5)
xC = pp.ClassifyPitchOpt(xM,xP);# xC = [i * 100 for i in xC]
zC = pp.ClassifyPitchOpt(zM,zP);# zC = [i * 100 for i in zC]
plt.subplot(121)
plt.title("x")
plt.plot(xM)
plt.plot(xC, 'k--', linewidth=0.7, label="X")
plt.subplot(122)
plt.plot(zM)
plt.title("z")
plt.plot(zC, 'k--', linewidth=0.7, label="Z")
plt.show()

# for hmm
i = 0
while i < len(normMotions):
    if i < len(normMotions):
        plt.subplot(221).set_title(files[i])
        xP = pp.CalcPitch(normMotions[i][0], 5)
        zP = pp.CalcPitch(normMotions[i][2], 5)
        #plt.plot(xP, '-', linewidth=1, label="xP")
        #plt.plot(zP, '-', linewidth=1, label="zP")
        plt.plot(pp.ClassifyPitch(xP), '-', linewidth=1, label="X")
        plt.plot(pp.ClassifyPitch(zP), '-', linewidth=1, label="Z")
        plt.legend(loc="upper left")
    if i+1 < len(normMotions):
        plt.subplot(222).set_title(files[i+1])
        xP = pp.CalcPitch(normMotions[i+1][0], 5)
        zP = pp.CalcPitch(normMotions[i+1][2], 5)
        #plt.plot(xP, '-', linewidth=1, label="xP")
        #plt.plot(zP, '-', linewidth=1, label="zP")
        plt.plot(pp.ClassifyPitch(xP), '-', linewidth=1, label="X")
        plt.plot(pp.ClassifyPitch(zP), '-', linewidth=1, label="Z")
    if i+2 < len(normMotions):
        plt.subplot(223).set_title(files[i+2])
        xP = pp.CalcPitch(normMotions[i+2][0], 5)
        zP = pp.CalcPitch(normMotions[i+2][2], 5)
        #plt.plot(xP, '-', linewidth=1, label="xP")
        #plt.plot(zP, '-', linewidth=1, label="zP")
        plt.plot(pp.ClassifyPitch(xP), '-', linewidth=1, label="X")
        plt.plot(pp.ClassifyPitch(zP), '-', linewidth=1, label="Z")
    if i+3 < len(normMotions):
        plt.subplot(224).set_title(files[i+3])
        xP = pp.CalcPitch(normMotions[i+2][0], 5)
        zP = pp.CalcPitch(normMotions[i+2][2], 5)
        #plt.plot(xP, '-', linewidth=1, label="xP")
        #plt.plot(zP, '-', linewidth=1, label="zP")
        plt.plot(pp.ClassifyPitch(xP), '-', linewidth=1, label="X")
        plt.plot(pp.ClassifyPitch(zP), '-', linewidth=1, label="Z")
    #plt.show()
    i +=4

i=0
dir = []
while i < len(normMotions):
    xP = pp.CalcPitch(normMotions[i][0],5)
    zP = pp.CalcPitch(normMotions[i][2],5)
    xPC = pp.ClassifyPitchOpt(normMotions[i][0],xP)
    zPC = pp.ClassifyPitchOpt(normMotions[i][2],zP)
    for j in range(len(xPC)):
        dir.append(pp.ClassifyDir(xPC[j], zPC[j]))
    print(dir)
    fo.DataToFile(path+"/directions",[dir], ['dir'], "dir_"+files[i])
    dir.clear()
    i += 1
