import numpy as np
import matplotlib.pyplot as plt
import hmms
import FileOps as fo
import random

def ForwardAlgo(Pi, b, a, o):
    nS = len(a)
    nO = len(o)

    f = np.zeros((nO, nS))#[[0]*nS]*nO
    for i in range(nS):
        f[0,i] = Pi[i] * b[0][o[0]]
    for t in range(1, nO):
        for i in range(nS):
            sum = 0
            for j in range(nS):
                sum += f[t-1,j]*a[j][i]
            sum *= b[i][o[t]]
            f[t,i] = sum
    P = 0
    for j in range(nS):
        P += f[nO-1,j]
    return P

Motions = ["1","2","3","4","5","6","kringel","N"]
directionData = []
for i in range(len(Motions)):
    path = ["../MotionData/" + Motions[i] + "/directions"]
    files, lMotionData = fo.getFilesFromFolder(path)
    directionData.append(lMotionData)

def PrintParams(dhmm):
    rows,cols = dhmm.a.shape
    for i in range(rows):
        for j in range(cols):
            print("&", "{0:0.4f}".format(dhmm.a[i,j]), end=' ')
        print()
    rows,cols = dhmm.b.shape
    for i in range(rows):
        for j in range(cols):
            print("&", "{0:0.4f}".format(dhmm.b[i,j]), end=' ')
        print()


    #print(dhmm.a)
    #print(dhmm.b)
    print(dhmm.pi)

#for Testing
Pi = np.array([1, 0, 0, 0])
A = np.array([[0.5, 0.5, 0.0, 0.0],
    [0.0, 0.5, 0.5, 0.0],
    [0.0, 0.0, 0.5, 0.5],
    [0.0, 0.0, 0.0, 1]])
B = np.array([[0.92, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01],
              [0.3, 0.01, 0.3, 0.01, 0.01, 0.01, 0.3, 0.01, 0.01],
              [0.3, 0.01, 0.01, 0.3, 0.01, 0.01, 0.01, 0.3, 0.01],
              [0.3, 0.3, 0.01, 0.01, 0.01, 0.3, 0.01, 0.01, 0.01]])
dhmm4 = hmms.DtHMM(A, B, Pi)
K4 = []; K3 = [];
for i in range(5):
    K4.append(directionData[3][i].values)
    K3.append(directionData[2][i].values)
K4 = np.array(K4, dtype=np.int32)
K4 = np.squeeze(K4)
K3 = np.array(K3, dtype=np.int32)
K3 = np.squeeze(K3)
print("Test without optimize: ")# , ForwardAlgo(Pi, B, A, K4))
for i in range(5):
    print("4.",i+1, ": ", ForwardAlgo(dhmm4.pi, dhmm4.b, dhmm4.a, K4[i]))
    print("3.",i+1, ": ", ForwardAlgo(dhmm4.pi, dhmm4.b, dhmm4.a, K3[i]))
dhmm4.baum_welch(K4[0:2])
for i in range(5):
    print("4.",i+1, ": ", ForwardAlgo(dhmm4.pi, dhmm4.b, dhmm4.a, K4[i]))
    print("3.",i+1, ": ", ForwardAlgo(dhmm4.pi, dhmm4.b, dhmm4.a, K3[i]))


PrintParams(dhmm4)

nEms = 9
nMinSeqs = 3
iterations = 35
TrainingSamples = 12

def GetRandomIdxs(maxNum):
    idxs = []
    iswap = 0
    for i in range(maxNum):
        idxs.append(i)
    for i in range(maxNum):
        i1 = random.randint(0, maxNum-1)
        i2 = random.randint(0, maxNum-1)
        iswap = idxs[i2]
        idxs[i2] = idxs[i1]
        idxs[i1] = iswap
    return idxs


dhmm = [hmms.DtHMM.random(nMinSeqs,nEms),hmms.DtHMM.random(nMinSeqs+1,nEms),hmms.DtHMM.random(nMinSeqs+2,nEms)]
#pi = [[0.9,0.1,0.1],[0.9,0.1,0.1,0.1],[0.9,0.1,0.1,0.1,0.1]]
EndDhmm = [hmms.DtHMM.random(nMinSeqs,nEms)]*len(Motions)

for h in range(len(Motions)):
    right = 0;
    lastRight = 0;
    wrong = 1000;
    lastWrong = 1000;
    WrongMax = 0

    for i in range(iterations):
        for j in range(len(dhmm)):
            #create random training data
            KTrainList = []
            idxs = GetRandomIdxs(len(directionData[h]))
            print(idxs)
            for trainidx in range(TrainingSamples):
                KTrainList.append(directionData[h][idxs[trainidx]].values)
            KTrain = np.array(KTrainList, dtype=np.int32)
            KTrain = np.squeeze(KTrain)

            dhmm[j] =  hmms.DtHMM.random(j+nMinSeqs,nEms)
            #dhmm[j].set_params(dhmm[j].a,dhmm[j].b,pi[j])
            dhmm[j].baum_welch(KTrain, 15)
            lEstRight = []
            lEstWrong = []
            for k in range(len(directionData)):
                for L in range(len(directionData[k])):
                    directionSample = np.squeeze(np.array(directionData[k][L].values,dtype=np.int32))
                    if k == h:
                        lEstRight.append(ForwardAlgo(dhmm[j].pi, dhmm[j].b, dhmm[j].a, directionSample))
                    else:
                        lEstWrong.append(ForwardAlgo(dhmm[j].pi, dhmm[j].b, dhmm[j].a, directionSample))
                if k != h:
                    wrong = np.mean(lEstWrong)
                    lEstWrong=[]
                    if wrong > WrongMax:
                        WrongMax = wrong
            right = np.mean(lEstRight)
            #wrong = np.mean(lEstWrong)
            if((right > lastRight and WrongMax <= lastWrong) or (right >= lastRight*10 and WrongMax <= lastWrong/3 ) or (right > lastRight/2 and WrongMax <= lastWrong/6)):
                EndDhmm[h] = dhmm[j]
                lastRight = right
                lastWrong = WrongMax
            WrongMax = 0

#hmms.print_parameters(EndDhmm)


for i in range(len(directionData)):
    print("\n", Motions[i], ":")
    for k in range(len(EndDhmm)):
        for j in range(len(directionData[i])):
            directionSample = np.squeeze(np.array(directionData[i][j].values,dtype=np.int32))
            print("Model", Motions[k], " / ",j , ": ", ForwardAlgo(EndDhmm[k].pi, EndDhmm[k].b, EndDhmm[k].a, directionSample))

EndDhmm[0].save_params("HMMModels/FinalHMM1")
EndDhmm[1].save_params("HMMModels/FinalHMM2")
EndDhmm[2].save_params("HMMModels/FinalHMM3")
EndDhmm[3].save_params("HMMModels/FinalHMM4")
EndDhmm[4].save_params("HMMModels/FinalHMM5")
EndDhmm[5].save_params("HMMModels/FinalHMM6")
EndDhmm[6].save_params("HMMModels/FinalHMMKr")
EndDhmm[7].save_params("HMMModels/FinalHMMN")

print("\n\n\n")
array = EndDhmm[0].a
for i in range(len(EndDhmm)):
    print("// Motion ", Motions[i])
    NumStates,nix = EndDhmm[i].a.shape
    Name = "nS_" + Motions[i]
    print("int " + Name + " = ", str(int(NumStates)), ";")
    for j in range(3):
        if j == 0:
            rows,cols = EndDhmm[i].a.shape
            array = EndDhmm[i].a
            VarName ="a_" + Motions[i] + "[" + str(rows*cols) + "]"
            #VarName ="a_" + Motions[i] + "[" + str(rows) + "][" + str(cols) + "]"
        elif j == 1:
            rows,cols = EndDhmm[i].b.shape
            array = EndDhmm[i].b
            #VarName ="b_" + Motions[i] + "[" + str(rows) + "][" + str(cols) + "]"
            VarName ="b_" + Motions[i] + "[" + str(rows*cols) + "]"
        elif j == 2:
            cols = np.squeeze(EndDhmm[i].pi.shape)
            rows = 1
            array = EndDhmm[i].pi
            array = np.reshape(array,(1,cols))
            #VarName ="pi_" + Motions[i] + "[" + str(rows) + "][" + str(cols) + "]"
            VarName ="pi_" + Motions[i] + "[" + str(rows*cols) + "]"

        print("float", VarName, "= {")
        for L in range(rows):
            #print("{", end = '')
            for k in range(cols):
                if k > 0:
                    print(",",array[L,k], end = '')
                elif k == 0:
                    print(array[L,k], end = '')
            if L < (rows-1):
                print(",")
            else:
                print("};")
                #print("}};\n")


"""
Motion = "1" #which Motion
paths = ["../MotionData/" + Motion + "/HMMtrainingData"]
files, lMotionData = fo.getFilesFromFolder(paths)
Motion = "3" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
files, lMotionData3 = fo.getFilesFromFolder(paths)

Pi1 = np.array([0.8,0.2])
A1 = np.array([[0.5,0.5],[0.1,0.9]])
B1 = np.array([[0.25, 0.04, 0.04, 0.25, 0.04, 0.04, 0.04, 0.26, 0.04],
               [0.2, 0.2, 0.15, 0.15, 0.05, 0.2, 0.05, 0.05, 0.05]])

#A1 = np.zeros((2,2), dtype=np.float); A1.fill(0.5)
#B1 = np.zeros((2,9), dtype=np.float); B1.fill(1/9)
K1 = np.array([lMotionData[0]["dir"], lMotionData[1]["dir"], lMotionData[2]["dir"], lMotionData[3]["dir"]], dtype=np.int32)
K3 = np.array([lMotionData3[0]["dir"], lMotionData3[1]["dir"], lMotionData3[2]["dir"], lMotionData3[3]["dir"]], dtype=np.int32)
S1 = np.array([lMotionData[0]["state"], lMotionData[1]["state"], lMotionData[2]["state"], lMotionData[3]["state"]], dtype=np.int32)

dhmm1 = hmms.DtHMM(A1, B1, Pi1)
hmms.print_parameters(dhmm1)
#print(K1[0])
#print(np.exp(dhmm1.emission_estimate(K1[0])))
#print(np.exp(dhmm1.estimate(S1[0], K1[0])))
#S1 = np.asanyarray(S1)
#K1 = np.asanyarray(K1)
print("K1 0: ",np.exp(dhmm1.emission_estimate(K1[0])))
print("K1 1: ",np.exp(dhmm1.emission_estimate(K1[1])))
print("K1 2: ",np.exp(dhmm1.emission_estimate(K1[2])))
print("K1 3: ",np.exp(dhmm1.emission_estimate(K1[3])))
print("K3 0: ",np.exp(dhmm1.emission_estimate(K3[0])))
print("K3 1: ",np.exp(dhmm1.emission_estimate(K3[1])))
print("K3 2: ",np.exp(dhmm1.emission_estimate(K3[2])))
print("K3 3: ",np.exp(dhmm1.emission_estimate(K3[3])))
dhmm1.baum_welch(K1[0:4],10)
#dhmm1.maximum_likelihood_estimation(S1, K1)
print("\n")
print("K1 0: ",np.exp(dhmm1.emission_estimate(K1[0])))
print("K1 1: ",np.exp(dhmm1.emission_estimate(K1[1])))
print("K1 2: ",np.exp(dhmm1.emission_estimate(K1[2])))
print("K1 3: ",np.exp(dhmm1.emission_estimate(K1[3])))
print("K3 0: ",np.exp(dhmm1.emission_estimate(K3[0])))
print("K3 1: ",np.exp(dhmm1.emission_estimate(K3[1])))
print("K3 2: ",np.exp(dhmm1.emission_estimate(K3[2])))
print("K3 3: ",np.exp(dhmm1.emission_estimate(K3[3])))
print("\n")
print("K1 0: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K1[0]))
print("K1 1: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K1[1]))
print("K1 2: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K1[2]))
print("K1 3: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K1[3]))
print("K3 0: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K3[0]))
print("K3 1: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K3[1]))
print("K3 2: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K3[2]))
print("K3 3: ",ForwardAlgo(dhmm1.pi, dhmm1.b, dhmm1.a, K3[3]))
"""