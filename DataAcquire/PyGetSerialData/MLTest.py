import pandas as pd
from pandas import DataFrame, Series, Timedelta, concat
from matplotlib import pyplot as plt
import os
import preprocessing as pp
import FileOps as fo
import numpy as np
import statistics
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from collections import defaultdict
from sklearn.metrics import confusion_matrix
from pandas.plotting import scatter_matrix
from sklearn.model_selection import GridSearchCV,train_test_split,cross_validate,cross_val_predict
from sklearn.metrics.classification import classification_report, f1_score, precision_score, recall_score


nix, X_Test1 = fo.getFilesFromFolder(["../MotionData/1/normData"]);Y_Test1 = [0] * len(X_Test1)
nix, X_Test2 = fo.getFilesFromFolder(["../MotionData/2/normData"]);Y_Test2 = [1] * len(X_Test2)
nix, X_Test3 = fo.getFilesFromFolder(["../MotionData/3/normData"]);Y_Test3 = [2] * len(X_Test3)
nix, X_Test4 = fo.getFilesFromFolder(["../MotionData/4/normData"]);Y_Test4 = [3] * len(X_Test4)
nix, X_Test5 = fo.getFilesFromFolder(["../MotionData/5/normData"]);Y_Test5 = [4] * len(X_Test5)
nix, X_Test6 = fo.getFilesFromFolder(["../MotionData/6/normData"]);Y_Test6 = [5] * len(X_Test6)
nix, X_TestKr = fo.getFilesFromFolder(["../MotionData/kringel/normData"]);Y_TestKr = [6] * len(X_TestKr)
nix, X_TestN = fo.getFilesFromFolder(["../MotionData/N/normData"]);Y_TestN = [7] * len(X_TestN)
X_TestR = []
Y_TestR = []
X_TestR.append(X_Test1.copy()); Y_TestR.extend(Y_Test1)
X_TestR.append(X_Test2.copy()); Y_TestR.extend(Y_Test2)
#X_TestR.append(X_Test3.copy()); Y_TestR.extend(Y_Test3)
#X_TestR.append(X_Test4.copy()); Y_TestR.extend(Y_Test4)
#X_TestR.append(X_Test5.copy()); Y_TestR.extend(Y_Test5)
#X_TestR.append(X_Test6.copy()); Y_TestR.extend(Y_Test6)
#X_TestR.append(X_TestKr.copy()); Y_TestR.extend(Y_TestKr)
#X_TestR.append(X_TestN.copy()); Y_TestR.extend(Y_TestN)
X_Test = []
for j in range(len(X_TestR)):
    for i in range(len(X_TestR[j])):
        buffer = X_TestR[j][i].drop(columns=['y'])
        buffer = buffer.values
        X_Test.append(buffer.reshape(-1))

Y_Test = Y_TestR.copy()

#print(X_TestR[0][0].head())


MotionNames = [ "1", "2"]#, "3","4", "5","6","kringel", "N"]
motionset = defaultdict(list)
for i in range(len(MotionNames)):
    path = ["../MotionData/" + MotionNames[i] + "/normData"]
    f, M = fo.getFilesFromFolder(path)
    #for j in range(len(M)):
    for j in range(12):
        MSet = M[j].drop(columns=['y'])
        #motionset[MotionNames[i]].append(MSet)
        motionset[i].append(MSet)

# give the amount of each figure and its stored motions
for figure, datasets in sorted(motionset.items()):
    print ("'{}' : {} recorded motions".format(figure, len(datasets)))


# vectors will contain all relevant feature vectors
vectors = []
# the corresponfing labels
labels = []
i = 0
testData = []
# loop over all motion sets
for figure, datasets in motionset.items():

    # for each data set ...
    for df in datasets:

        vectors.append(df.values.reshape(-1))
        labels.append(figure)
        i +=1
        if i == 8:
            testData = df.values.reshape(-1)

#print("double TestData[",len(testData),"] = {")
#for i in range(len(testData)):
    #print("%.1f" % testData[i], end = '')
    #print(",", end = '')
#print("};")

X = DataFrame(vectors)
Y = Series(labels)
print(X.shape, Y.shape)


parameter_candidates = [
  {'C': [0.01,0.05,0.1, 1, 10, 100], 'kernel': ['linear']},
  {'C': [0.01,0.05,0.1, 1, 10, 100], 'gamma': [0.05,0.01,0.005,0.001,0.0001], 'kernel': ['rbf']},
  {'C': [0.01,0.05,0.1, 1, 10, 100], 'degree': [1, 2, 3, 4], 'kernel': ['poly']}
]

# Create a classifier object with the classifier and parameter candidates
clf = GridSearchCV(estimator=SVC(), param_grid=parameter_candidates, n_jobs=-1, cv=3)
# Train the classifier on data1's feature and target data
clf.fit(X, Y)

params_forest = [
    {'n_estimators': [10,20,30,40, 50, 60, 70, 100, 150, 200, 250]},
]

params_linear = [
  {'C': [0.01,0.05,0.1, 1, 10, 100]}
]

clfForest = GridSearchCV(estimator=RandomForestClassifier(), param_grid=params_forest, n_jobs=-1, cv=3)
clfLinSVC = GridSearchCV(estimator=LinearSVC(), param_grid=params_linear, n_jobs=-1, cv=3)
clfForest.fit(X, Y)
clfLinSVC.fit(X, Y)

print('BestParamsForest:', clfForest.best_params_)
print('BestParamsLinear:', clfLinSVC.best_params_)
# View the accuracy score
print('Best score for data1:', clf.best_score_)

# View the best parameters for the model found using grid search
print('Best C:',clf.best_estimator_.C)
print('Best Kernel:',clf.best_estimator_.kernel)
print('Best Gamma:',clf.best_estimator_.gamma)
print('Best Degree:',clf.best_estimator_.degree)
print('BestParams:', clf.best_params_)


svm = clfLinSVC.best_estimator_ #LinearSVC(C=0.01)
forest = clfForest.best_estimator_ #RandomForestClassifier(n_estimators=150)
#svm2 = SVC(C=0.01, kernel='linear')#'#C=1., random_state=0)

#y_pred_cv = cross_validate(svm, X_Test, Y_Test, cv=3, return_estimator=True)
#print(y_pred_cv.get('estimator'))
#print(classification_report(Y_Test, y_pred_cv))
#y_pred_cv = cross_validate(forest, X_Test, Y_Test, cv=3, return_estimator=True)
#forest = y_pred_cv.estimator.best_estimator_
#print(classification_report(Y_Test, y_pred_cv))
#y_pred_cv = cross_validate(svm2, X_Test, Y_Test, cv=3, return_estimator=True)
#svm2 = y_pred_cv.estimator.best_estimator_
#print(classification_report(Y_Test, y_pred_cv))


decisionT = DecisionTreeClassifier()
svm.fit(X,Y)
#svm2.fit(X,Y)
forest.fit(X,Y)
decisionT.fit(X,Y)
y_pred = svm.predict(X)

labels=sorted(motionset.keys())
C = confusion_matrix(Y, y_pred, labels=labels)
#print(C)

#print(X_Test.shape)
#print(Y_Test.shape)
#Y_Test = Series(Y_TestR)

print("GridSearch\n",DataFrame(confusion_matrix(Y_Test, clf.predict(X_Test)), columns=MotionNames, index=MotionNames), '\n')
print("LinearSVM\n",DataFrame(confusion_matrix(Y_Test, svm.predict(X_Test)), columns=MotionNames, index=MotionNames), '\n')
#print("SVM\n",DataFrame(confusion_matrix(Y_Test, svm2.predict(X_Test)), columns=MotionNames, index=MotionNames), '\n')
print("Random Forest\n",DataFrame(confusion_matrix(Y_Test, forest.predict(X_Test)), columns=MotionNames, index=MotionNames), '\n')
print("Decision Tree\n",DataFrame(confusion_matrix(Y_Test, decisionT.predict(X_Test)), columns=MotionNames, index=MotionNames), '\n')
print(svm.predict(X_Test))
#print(svm2.predict(X_Test))
print(forest.predict(X_Test))
print(decisionT.predict(X_Test))




from sklearn_porter import Porter

porter = Porter(svm, language='c')
output = porter.export()
print(output)
porter = Porter(clf.best_estimator_, language='c')
output = porter.export()
print(output)
porter = Porter(forest, language='c')
output = porter.export()
print(output)








"""
XScat = []
for j in range(len(X_TestR)):
    for i in range(len(X_TestR[j])):
        classList = [j] * len(X_TestR[j][i])
        bufferScat = X_TestR[j][i].drop(columns=['y'])
        DataIloc = bufferScat.iloc[25]
        DataIloc['target'] = int(j)
        DataIloc['class'] = MotionNames[j]
        XScat.append(DataIloc.values)
names = ['x', 'z', 'target', 'class']
df = DataFrame(
    data = XScat,
    columns = names
)

print(df.loc[:])

#scatter_matrix(df, c=df.target)

X=df[['x','z']]

# make a grid
x1 = X_Test[0]
x2 = X_Test[10]
X1,X2 = np.meshgrid(x1,x2)
x_grid = np.array(list(zip(X1.ravel(), X2.ravel())))

Y_bin = (df.target == 2)
#scatter_matrix(X, c=Y_bin);

plt.scatter(X_Test[0], X_Test[10], marker='.', s=1);
#plt.scatter(X.iloc[:,0], X.iloc[:,1], c=df.target, marker='d');
plt.show()
"""