import hidden_markov as hmm
import numpy as np

S = ["N", "O", "S", "W"]
K = [1,3,5,7]
K1 = [1,1,3,3,3,5,5,5,5,5,7,7,7,1,1]
K2 = [1,3,3,5,5,5,7,7,7,1,1]
K3 = [1,1,1,1,3,3,3,5,5,5,5,7,7,7]
#K3 = [2,2,2,2,5,5,5,5,5]
Pi = np.matrix('0.6, 0.2,0.1,0.1')
#A = np.matrix('0.25,0.25,0.25,0.25 ; 0.25,0.25,0.25,0.25 ; 0.25,0.25,0.25,0.25 ; 0.25, 0.25, 0.25, 0.25')
#B = np.matrix('0.125,0.125,0.125,0.125,0.125,0.125,0.125,0.125 ; 0.125,0.125,0.125,0.125,0.125,0.125,0.125,0.125')

A = np.matrix('0.3, 0.6,0.05,0.05; '
              '0.05, 0.35, 0.55, 0.05;'
              '0.05, 0.55, 0.35, 0.05;'
              '0.45, 0.05, 0.05, 0.45')

B = np.matrix('0.3, 0.6,0.05,0.05; '
              '0.05, 0.35, 0.55, 0.05;'
              '0.05, 0.55, 0.35, 0.05;'
              '0.45, 0.05, 0.05, 0.45')

HMM = hmm.hmm(S,K,Pi,A,B)

print(HMM.forward_algo(K1))

observations = [K1,K2]
quants = [2, 1]

Km, Am, Pim = HMM.train_hmm(observations, 10000, quants)
print(Km)
print(Am)
print(Pim)
test = hmm.hmm(S,K,Pim,Am,Km)
print(test.forward_algo(K1))
print(test.forward_algo(K2))
print(test.forward_algo(K3))
#test = hmm.hmm()