import os
import pandas as pd
import time
from time import gmtime, strftime

def getFilesFromFolder(path):
    lMotionData = []
    folder = []
    files = []
    for i in range(0, len(path)):
        folder = os.listdir(path[i])
        for j in range(0, len(folder)):
            files.append(folder[j])
            folder[j] = path[i] + '/' + folder[j]
            lMotionData.append(pd.read_csv(folder[j]))
    return files, lMotionData

def GetDataFromFiles (dirPath, lAttributes):
    files, lMotionData = getFilesFromFolder(dirPath)
    retList = []
    bufList = []
    for i in range(len(lAttributes)):
        for j in range(len(lMotionData)):
            bufList.append(lMotionData[j][lAttributes[i]])
        retList.append(bufList.copy()); bufList.clear()
    return retList

def DataToFile(Path, lValues, lColNames, sPrefix):
    if len(lValues) != len(lColNames):
        print("error, need matching format in lists")
        return

    dirPath = Path# + "/DTWSeq"
    try:
        os.mkdir(dirPath)
    except OSError:
        print("ne")
    else:
        print("success")

    sTime = strftime("%Y_%m_%d %H_%M_%S", time.localtime())
    Filename = sPrefix
    cols = len(lColNames)
    rows = len(lValues[0])
    with open(dirPath + '/' + Filename, 'w') as f:
        for i in range(cols):
            f.write(lColNames[i])
            if i < cols -1:
                f.write(',')
        f.write('\n')

        for i in range(rows):
            for j in range(cols):
                f.write("%.2f" % lValues[j][i])
                if j < cols -1:
                    f.write(',')
            f.write('\n')
    print("wrote to file %s\n" % Filename)