import numpy as np
import matplotlib.pyplot as plt
import hmms
import FileOps as fo


Motion = "1" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
files, lMotionData = fo.getFilesFromFolder(paths)
Motion = "3" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
files, lMotionData3 = fo.getFilesFromFolder(paths)
Motion = "kringel" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
files, lMotionDataKr = fo.getFilesFromFolder(paths)



#A1 = np.zeros((2,2), dtype=np.float); A1.fill(0.5)
#B1 = np.zeros((2,9), dtype=np.float); B1.fill(1/9)

Motion = "1" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
K1 = np.asarray(fo.GetDataFromFiles(paths, ["dir"])[0], dtype=np.int32)
Motion = "3" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
K3 = np.asarray(fo.GetDataFromFiles(paths, ["dir"])[0], dtype=np.int32)
Motion = "kringel" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
KKr = np.asarray(fo.GetDataFromFiles(paths, ["dir"])[0], dtype=np.int32)
Motion = "Test" #which Motion
paths = ["../MotionData/" + Motion + "/directions"]
KTest = np.asarray(fo.GetDataFromFiles(paths, ["dir"])[0], dtype=np.int32)

#K1 = np.array([lMotionData[0]["dir"], lMotionData[1]["dir"], lMotionData[2]["dir"], lMotionData[3]["dir"]], dtype=np.int32)
#K3 = np.array([lMotionData3[0]["dir"], lMotionData3[1]["dir"], lMotionData3[2]["dir"], lMotionData3[3]["dir"]], dtype=np.int32)
#KKr = np.array([lMotionDataKr[0]["dir"], lMotionDataKr[1]["dir"], lMotionDataKr[2]["dir"], lMotionDataKr[3]["dir"]], dtype=np.int32)
#S1 = np.array([lMotionData[0]["state"], lMotionData[1]["state"], lMotionData[2]["state"], lMotionData[3]["state"]], dtype=np.int32)


avgEstEMax = 1e-90
avgEstIMax = 1e-90

nSeq = 3
nEms = 9

dhmmEI = hmms.DtHMM.random(nSeq,nEms)
dhmmEE = hmms.DtHMM.random(nSeq,nEms)
dhmmII = hmms.DtHMM.random(nSeq,nEms)
dhmmIE = hmms.DtHMM.random(nSeq,nEms)
pi = np.array((1,0.1,0.1))

KTrain = K1
for i in range(0):
    dhmmTest = hmms.DtHMM.random(nSeq,nEms)
    dhmmTest.set_params(dhmmTest.a, dhmmTest.b, pi)
    avgEstI = np.exp(sum([dhmmTest.emission_estimate(x) for x in KTrain])/len(KTrain))
    #avgEstI = np.exp((dhmmTest.emission_estimate(K3[0])+dhmmTest.emission_estimate(K3[1])+
    #           dhmmTest.emission_estimate(K3[2])+dhmmTest.emission_estimate(K3[3]))/4)
    AI = dhmmTest.a
    BI = dhmmTest.b
    PI = dhmmTest.pi

    dhmmTest.baum_welch(KTrain,10)
    avgEstE = np.exp(sum([dhmmTest.emission_estimate(x) for x in KTrain])/len(KTrain))
    #avgEstE = np.exp((dhmmTest.emission_estimate(K3[0])+dhmmTest.emission_estimate(K3[1])+
    #           dhmmTest.emission_estimate(K3[2])+dhmmTest.emission_estimate(K3[3]))/4)
    AE = dhmmTest.a
    BE = dhmmTest.b
    PE = dhmmTest.pi

    if avgEstE > avgEstEMax:
        dhmmEI = hmms.DtHMM(AI,BI,PI)
        dhmmEE = hmms.DtHMM(AE,BE,PE)
        avgEstEMax = avgEstE
        avgEstIMax = avgEstI
    print(i)

print("InitValue: ",avgEstIMax)
print("bestEndValue: ",avgEstEMax)
print("EI");hmms.print_parameters(dhmmEI);
print("EE");hmms.print_parameters(dhmmEE);
#dhmmEI.save_params("randomHMMEI")
#dhmmEE.save_params("randomHMM1_0605")

def TestHMMParams(dhmmTest, lK, KName):
    for i in range(len(lK)):
        print("{}[{}]: {}".format(KName, i, np.exp(dhmmTest.emission_estimate(lK[i]))))


print("Model1")
dhmm1 = hmms.DtHMM.from_file( "randomHMM1_0605.npz" )
TestHMMParams(dhmm1, K1, "1")
TestHMMParams(dhmm1, K3, "3")
TestHMMParams(dhmm1, KKr, "Kringel")
TestHMMParams(dhmm1, KTest, "Test")
print("\nModel3")
dhmm3 = hmms.DtHMM.from_file( "randomHMM3_1.npz" )
TestHMMParams(dhmm3, K1, "1")
TestHMMParams(dhmm3, K3, "3")
TestHMMParams(dhmm3, KKr, "Kringel")
TestHMMParams(dhmm3, KTest, "Test")
print("\nModel Kringel")
dhmmKr = hmms.DtHMM.from_file( "randomHMMKringel_2.npz" )
TestHMMParams(dhmmKr, K1, "1")
TestHMMParams(dhmmKr, K3, "3")
TestHMMParams(dhmmKr, KKr, "Kringel")
TestHMMParams(dhmmKr, KTest, "Test")

#( log_prob, s_seq1 ) =  dhmmKr.viterbi( KKr[0] )
#( log_prob, s_seq2 ) =  dhmmKr.viterbi( KKr[1] )
#( log_prob, s_seq3 ) =  dhmmKr.viterbi( KKr[4] )
#( log_prob, s_seq4 ) =  dhmmKr.viterbi( KKr[6] )
# Let's print the most likely state sequence, it can be same or differ from the sequence above.
#hmms.plot_hmm( s_seq1, KKr[0] )
#hmms.plot_hmm( s_seq2, KKr[1] )
#hmms.plot_hmm( s_seq3, KKr[2] )
#hmms.plot_hmm( s_seq4, KKr[3] )