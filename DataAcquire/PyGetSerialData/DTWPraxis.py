import numpy as np
from matplotlib import pyplot as plt
import time
from matplotlib import colors
import matplotlib.gridspec as gridspec
import FileOps as fo

def CalcDTWValues(pathArr, DTWVec, TestInput1):
    for i in range(len(DTWVec)):
        for j in range(len(TestInput1)):
            pathArr[i][j] = (DTWVec[i] - TestInput1[j])**2
    return pathArr

def CalcDTWValuesOpt(pathArr, DTWVec, TestInput1):
    f = len(TestInput1) / len(DTWVec)
    for i in range(len(DTWVec)):
        for j in range(len(TestInput1)):
            fi = f*i
            if j > fi-20 and j < fi+20:
                pathArr[i][j] = (DTWVec[i] - TestInput1[j])**2
    return pathArr

def checkMin(pathArr, i, j):

    if pathArr[i[0]][j[0]] <= pathArr[i[1]][j[1]] and pathArr[i[0]][j[0]] <= pathArr[i[2]][j[2]]:
        return i[0],j[0]

    if pathArr[i[1]][j[1]] <= pathArr[i[0]][j[0]] and pathArr[i[1]][j[1]] <= pathArr[i[2]][j[2]]:
        return i[1],j[1]

    if pathArr[i[2]][j[2]] <= pathArr[i[1]][j[1]] and pathArr[i[2]][j[2]] <= pathArr[i[0]][j[0]]:
        return i[2],j[2]


def AccumulatedCostMatrix(pathArr, leni, lenj):
    dtw = pathArr.copy()
    dtw.fill(1000000)
    dtw[0,0] = 0
    n = leni
    m = lenj

    for i in range(1,n):
        if pathArr[i,0] < 10000:
            dtw[i,0] = dtw[i-1,0]+pathArr[i,0]
    for j in range(1,m):
        if pathArr[0,j] < 10000:
            dtw[0,j] = dtw[0,j-1]+pathArr[0,j]
    for i in range(1,n):
        for j in range(1,m):
            if pathArr[i,j] < 10000:
                dtw[i,j] = pathArr[i,j] + dtw[checkMin(dtw,[i, i-1, i-1], [j-1, j-1, j])]
    return dtw

def FindWarpingPathBackwards(Origin, dtw, leni, lenj):
    PathListX = []
    PathListY = []
    ValList = []
    PathLength = 0

    i = leni; j = lenj
    while i > 1 or j > 1:
        PathListX.insert(0, j)
        PathListY.insert(0, i)
        ValList.insert(0, Origin[i,j])
        PathLength += Origin[i,j]
        if i >= 1 and j >= 1:
            i, j = checkMin(dtw, [i, i-1, i-1], [j-1, j-1, j])
        elif i == 0:
            j -= 1
        elif j == 0:
            i -= 1
        else:
            continue
    return PathListX, PathListY, ValList, PathLength

lGestures = ["1", "2", "3", "4", "5", "6", "kringel", "N","Negative"]
lDTWModels = []
lDTW_X = []; lDTW_Z = []
for i in range(len(lGestures)):
    DtwPath = "../MotionData/" + lGestures[i] + "/DTWSeq";
    nix, lDTWModel = fo.getFilesFromFolder([DtwPath])
    lDTWModels.append(lDTWModel)
    lDTWModel = lDTWModel[0].values
    lDTW_X.append(lDTWModel[:,0])
    lDTW_Z.append(lDTWModel[:,1])
lTestData = []; lNormData =[]
for i in range(len(lGestures)):
    normPath = "../MotionData/" + lGestures[i] + "/normData"
    nix, lNormDataFiles = fo.getFilesFromFolder([normPath])
    for j in range(len(lNormDataFiles)):
        lNormDataFiles[j] = lNormDataFiles[j].drop(columns=['y'])
        lNormData.append(lNormDataFiles[j].values)
    lTestData.append(lNormData.copy())
    lNormData.clear()


pathArrX = np.zeros((50,50), dtype=np.float32)
pathArrZ = np.zeros((50,50), dtype=np.float32)

maxIdx = 49
maxVal = 17000

testIdx = 2

pathArrX = CalcDTWValues(pathArrX, lDTW_X[0], lTestData[0][testIdx][:, 0])
dtw = AccumulatedCostMatrix(pathArrX, maxIdx, maxIdx)
LXX, LYX, ValuesX, LengthX = FindWarpingPathBackwards(pathArrX, dtw, maxIdx, maxIdx)
LXX.insert(0, 0); LXX.insert(1, 1)
LYX.insert(0, 0); LYX.insert(1, 1)

#lDTWPlt = [-1 * i for i in lDTWPlt]
lTestDataX = []
lTestDataY = []
for i in range(50):
    lTestDataY.append(i)
    lTestDataX.append(max(lTestData[0][testIdx][:, 0])-lTestData[0][testIdx][i, 0])

gridColor = []
for i in range(50):
    gridColor.append([[255,255,255]]*50)
for i in range(50):
    for j in range(50):
        if j > i+15:
            gridColor[i][j]=[100,100,100]
        if j < i-15:
            gridColor[i][j]=[100,100,100]

gridColor = np.array(gridColor)
cmap = colors.ListedColormap(['#FFFFFF','#222222'])
bounds = [1,2]#,5,100,500]
norm = colors.BoundaryNorm(bounds, cmap.N)

gs = gridspec.GridSpec(4,4)
axUp = plt.subplot(gs[0, 1:])
plt.xticks([0, 10, 20, 30, 40, 49]);plt.yticks([]);plt.box(True)
axUp.plot(lDTW_X[0])
axUp.set_xlim(xmin=0, xmax=49)
axUp.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=True,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=True) # labels along the bottom edge are off
axUp.set_xticklabels(['0', '10', '20', '30', '40', '50'])

axLeft = plt.subplot(gs[1:, 0])
plt.yticks([0, 10, 20, 30, 40, 49]);plt.xticks([]);plt.box(True)
axLeft.plot(lTestDataX, lTestDataY)
axLeft.set_ylim(ymin=0, ymax=49)
axUp.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    right=True,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelright=True) # labels along the bottom edge are off
axLeft.set_yticklabels(['0', '10', '20', '30', '40', '50'])

ax = plt.subplot(gs[1:, 1:])
#fig, ax = plt.subplots();
ax.imshow(gridColor, aspect='auto')#, cmap=cmap, norm=norm)
ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=0.4)
ax.set_xticks(np.arange(0, 50, 1));
ax.set_yticks(np.arange(0, 50, 1));
ax.set_ylim(ymin=0, ymax=49)
ax.set_xlim(xmin=0, xmax=49)
ax.plot(LXX, LYX, color='black', linewidth=0.6)
plt.scatter(LXX,LYX, marker='.', s=35, color='black')
ax.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=True) # labels along the bottom edge are off
ax.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off
#lxTicks = []*50; lYTicks = []*50
#lxTickl = [' ']*50; lYTickl = [' ']*50
ax.set_yticklabels([])
ax.set_xticklabels([])
#plt.text(LXX[1], LYX[1]-1, "w1")
#plt.text(LXX[2], LYX[2]-1, "w2")
#plt.text(LXX[3]+1, LYX[3], "w3")
#plt.text(LXX[-1], LYX[-1], "wk")
plt.show()

"""
right = 0; wrong = 0;
TP=0; TN=0; FP=0; FN=0
print("Gesture idx ")
for i in range(len(lTestData)):
    print(i,",", end = '' )
    for j in range(len(lTestData[i])):
        minVal = 999999; idx = 99
        for k in range(len(lGestures)):
            pathArrX = CalcDTWValues(pathArrX, lDTW_X[k], lTestData[i][j][:,0])
            dtw = AccumulatedCostMatrix(pathArrX, maxIdx, maxIdx)
            LX, LY, Values, LengthX = FindWarpingPathBackwards(pathArrX, dtw, maxIdx, maxIdx)

            pathArrZ = CalcDTWValues(pathArrZ, lDTW_Z[k], lTestData[i][j][:,1])
            dtw = AccumulatedCostMatrix(pathArrZ, maxIdx, maxIdx)
            LX, LY, Values, LengthZ = FindWarpingPathBackwards(pathArrZ, dtw, maxIdx, maxIdx)
            if LengthX < maxVal and LengthZ < maxVal :
                if minVal > (LengthX + LengthZ) :
                    minVal = (LengthX + LengthZ)
                    idx = k
        if i != 8:
            if idx == i:
                right += 1
            else:
                wrong += 1


            if idx == 99:
                FN +=1
            else:
                TP += 1
        else:
            if idx == 99:
                TN += 1
            else:
                FP += 1
print("\n")
print("right: ", right, ", wrong: ", wrong)
print("TP: ", TP,", TN: ", TN,", FP: ", FP,", FN: ", FN)
"""

nix, X_Test1 = fo.getFilesFromFolder(["../MotionData/1/DTWSeq"])
nix, X_Test2 = fo.getFilesFromFolder(["../MotionData/2/DTWSeq"])
nix, X_Test3 = fo.getFilesFromFolder(["../MotionData/3/DTWSeq"])
nix, X_Test4 = fo.getFilesFromFolder(["../MotionData/4/DTWSeq"])
nix, X_Test5 = fo.getFilesFromFolder(["../MotionData/5/DTWSeq"])
nix, X_Test6 = fo.getFilesFromFolder(["../MotionData/6/DTWSeq"])
nix, X_TestKr = fo.getFilesFromFolder(["../MotionData/kringel/DTWSeq"])
nix, X_TestN = fo.getFilesFromFolder(["../MotionData/N/DTWSeq"])

X1 = X_Test1[0].values
X2 = X_Test2[0].values
X3 = X_Test3[0].values
X4 = X_Test4[0].values
X5 = X_Test5[0].values
X6 = X_Test6[0].values
XKr = X_TestKr[0].values
XN = X_TestN[0].values

nix, X_Test1 = fo.getFilesFromFolder(["../MotionData/1/normData"]);Y_Test1 = ['1'] * len(X_Test1)
nix, X_Test3 = fo.getFilesFromFolder(["../MotionData/3/normData"]);Y_Test3 = ['3'] * len(X_Test3)
nix, X_TestKr = fo.getFilesFromFolder(["../MotionData/kringel/normData"]);Y_TestKr = ['kringel'] * len(X_TestKr)
nix, X_Test4 = fo.getFilesFromFolder(["../MotionData/4/normData"]);Y_Test4 = ['4'] * len(X_Test4)
nix, X_Test6 = fo.getFilesFromFolder(["../MotionData/6/normData"]);Y_Test6 = ['6'] * len(X_Test6)

#print DTW_Values
print("float dtwN_x[50] = {", end = '')
for i in range(50):
    if i == 0:
        print(XN[i][0], end = '')
    else:
        print(",",XN[i][0], end = '')
print("};")
print("float dtwN_z[50] = {", end = '')
for i in range(50):
    if i == 0:
        print(XN[i][1], end = '')
    else:
        print(",",XN[i][1], end = '')
print("};")

pathArrX = np.zeros((len(X1[:,0]), len(X_Test1[0]['x'])), dtype=np.float32)
pathArrZ = np.zeros((len(X1[:,1]), len(X_Test1[0]['z'])), dtype=np.float32)

#plt.plot(X1[:,0])
#plt.plot(X_Test1[0]['x'])
#plt.show()

pathArrX = CalcDTWValues(pathArrX, X1[:,0], X_Test1[0]['x'])
pathArrZ = CalcDTWValues(pathArrZ, X1[:,1], X_Test1[0]['z'])

print("XLength, XLengthV, ZLength, ZLengthV")
for i in range(len(X_Test1)):
    pathArrX = CalcDTWValues(pathArrX, X1[:,0], X_Test1[i]['x'])
    dtw = AccumulatedCostMatrix(pathArrX, len(X1[:,1])-1, len(X_Test1[i]['x'])-1)
    LX, LY, Values, Length = FindWarpingPathBackwards(pathArrX, dtw, len(X1[:,1])-1, len(X_Test1[0]['x'])-1)
    print(len(Values), ",", Length, end = '')
    plt.subplot(121);plt.plot(LX,LY)

    pathArrZ = CalcDTWValues(pathArrZ, X1[:, 1], X_Test1[i]['z'])
    dtw = AccumulatedCostMatrix(pathArrZ, len(X1[:,1])-1, len(X_Test1[i]['z'])-1)
    LX, LY, Values, Length = FindWarpingPathBackwards(pathArrZ, dtw, len(X1[:,1])-1, len(X_Test1[0]['z'])-1)
    print(",",len(Values), ",", Length)
    plt.subplot(122);plt.plot(LX,LY)
plt.show()
#print("Length: ", len(Values), " LengthV:", Length)
