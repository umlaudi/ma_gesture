import numpy as np
from matplotlib import pyplot as plt
import time
from matplotlib import colors

#TestInput1 = [-5.5, -5.0, -5.4, -4.6, -6.4, -8.0, -3.0, -2.7, -9.2, -11.4, -6.9, -6.1, -10.0, -9.8, -5.6, -0.9, 2.2, 0.2, -8.5, -15.0, -14.7, -16.9, -18.1, -6.2, 8.6, 21.5, 38.3, 54.2, 66.1, 75.3, 83.6, 88.2, 88.5, 91.9, 94.3, 95.4, 94.8, 97.4, 98.4, 100.0, 98.6, 96.2, 90.4, 82.4, 76.5, 65.4, 56.3, 51.0, 45.0, 30.1, 21.5, 15.6, 1.7, -12.1, -18.3, -25.0, -37.5, -38.9, -41.5, -48.3, -50.2, -50.5, -56.5, -59.9, -58.9, -63.8, -69.0, -66.0, -59.2, -60.6, -65.6, -67.1, -64.3, -71.9, -74.3, -68.1, -61.2, -60.4, -58.0, -48.0, -40.1, -41.3, -37.2, -22.8, -13.0, -9.1, -0.1, 9.6, 11.3, 14.8, 23.5, 28.1, 30.0, 36.7, 41.7, 40.9, 41.3, 41.4, 42.9, 40.6]
#TestInput1 = [-12.2, 6.3, 5.7, 1.8, -0.4, 3.7, 4.8, 1.1, -0.4, 0.8, -0.1, -1.0, -1.3, -3.2, -3.9, -4.2, -5.4, -4.1, -2.2, -0.8, 1.1, 3.3, 4.6, 11.5, 18.0, 19.1, 16.3, 21.2, 25.8, 28.1, 35.2, 44.4, 49.0, 50.4, 59.6, 68.8, 71.8, 83.3, 87.9, 84.3, 85.9, 90.6, 94.4, 95.7, 100.0, 97.9, 92.7, 91.7, 89.8, 84.6, 81.1, 78.0, 70.7, 63.2, 55.5, 42.8, 33.6, 28.7, 20.2, 11.9, 5.0, -0.7, -9.4, -17.2, -19.9, -24.5, -34.5, -38.1, -33.7, -37.7, -47.4, -52.4, -56.1, -64.2, -69.2, -68.8, -68.5, -67.9, -62.5, -57.2, -51.9, -48.8, -43.8, -39.0, -33.2, -27.3, -20.7, -15.3, -10.6, -2.8, 6.8, 14.6, 20.1, 29.5, 39.7, 44.3, 39.2, 37.0, 42.3, 49.4]
#TestInput1 = [4.8, -7.1, -7.9, -5.3, -5.0, -5.9, -8.2, -8.4, -7.8, -7.2, -7.8, -8.6, -8.4, -5.5, 0.7, 8.3, 14.2, 18.5, 22.4, 27.1, 30.6, 33.2, 36.0, 39.3, 44.6, 47.2, 51.5, 55.0, 58.1, 60.1, 63.9, 69.6, 75.5, 79.6, 83.1, 85.2, 87.7, 91.5, 95.1, 97.4, 98.7, 99.9, 100.0, 98.5, 96.4, 93.6, 90.9, 85.9, 78.6, 70.3, 67.8, 61.8, 53.0, 42.1, 39.9, 36.0, 26.6, 17.1, 13.8, 10.7, 2.1, -5.7, -7.8, -9.1, -15.1, -22.6, -27.0, -29.2, -31.3, -36.0, -38.2, -39.6, -37.5, -37.6, -42.6, -47.5, -50.9, -51.7, -56.1, -60.7, -58.5, -52.7, -49.1, -49.9, -49.5, -43.1, -35.5, -30.0, -26.4, -21.1, -14.0, -7.6, -1.4, 6.5, 13.8, 21.4, 27.8, 36.2, 44.4, 48.1]
from skimage import color

TestInput1 = [14.2, -9.9, -9.3, -12.1, -10.0, -7.0, -3.7, -3.8, -3.9, -5.5, -8.3, -12.5, -15.6, -17.3, -18.2, -16.5, -11.6, -4.4, 2.6, 9.8, 16.5, 23.7, 31.3, 36.7, 42.7, 46.5, 55.5, 62.7, 69.2, 72.9, 76.9, 81.7, 85.8, 88.8, 90.8, 92.7, 94.5, 95.7, 96.8, 97.7, 98.9, 100.0, 98.8, 96.2, 93.2, 92.8, 92.1, 89.3, 83.8, 82.0, 79.5, 75.6, 67.5, 59.3, 54.3, 48.8, 43.6, 35.6, 28.5, 24.6, 21.9, 17.0, 9.9, 3.3, -1.8, -7.4, -15.0, -21.6, -23.5, -23.9, -27.0, -32.6, -36.8, -38.0, -38.0, -41.0, -45.6, -48.9, -50.3, -51.3, -55.8, -58.7, -57.7, -54.3, -54.6, -56.0, -54.2, -47.3, -39.8, -36.4, -35.4, -32.5, -24.0, -13.1, -5.0, 0.4, 7.9, 20.1, 33.7, 39.3]

#without interpolation
#TestInput1 = [-5.4, -4.7, -6.1, -4.2, -4.9, -7.0, -8.0, -3.2, -1.0, -7.0, -11.7, -11.2, -5.9, -6.1, -9.8, -10.7, -8.2, -3.4, 0.1, 2.4, 0.9, -6.0, -15.0, -15.0, -14.6, -17.4, -18.1, -8.5, 4.4, 16.0, 26.0, 42.8, 55.3, 65.3, 73.0, 81.3, 86.1, 89.2, 88.2, 91.8, 93.9, 95.8, 94.4, 95.0, 98.1, 98.3, 100.0, 98.7, 97.7, 94.2, 88.0, 81.1, 76.4, 66.7, 58.9, 51.7, 50.3, 42.9, 28.8, 21.8, 17.9, 9.3, -7.3, -14.8, -19.1, -24.9, -37.1, -39.0, -38.6, -43.8, -49.9, -50.2, -50.0, -54.8, -60.5, -59.0, -58.7, -64.9, -68.9, -67.2, -60.0, -57.6, -63.0, -66.4, -67.0, -63.6, -70.9, -74.1, -74.3, -64.4, -60.4, -60.4, -59.3, -51.6, -41.6, -38.8, -42.1, -36.7, -23.7, -13.8, -10.8, -7.0, 3.9, 10.9, 11.3, 13.3, 21.3, 27.2, 28.8, 30.3, 37.3, 41.7, 40.9, 40.9, 41.6, 41.3, 43.2, 40.6]
#TestInput1 = [-12.2, 6.3, 6.3, 5.2, 1.1, -0.6, 2.5, 5.9, 4.2, 0.8, -0.7, 1.0, 0.5, -0.2, -1.0, -0.8, -2.7, -3.6, -3.9, -4.1, -5.6, -5.2, -3.4, -2.0, -1.2, 0.6, 1.6, 3.9, 4.6, 9.8, 16.2, 19.4, 19.0, 16.0, 19.7, 23.8, 27.0, 28.2, 33.8, 41.5, 47.8, 49.4, 50.4, 57.4, 65.4, 71.7, 71.8, 83.0, 86.4, 90.4, 80.7, 86.4, 89.7, 94.9, 93.8, 96.4, 100.0, 99.0, 94.8, 90.9, 91.9, 90.2, 86.6, 81.0, 81.2, 77.7, 72.0, 64.7, 61.4, 53.3, 42.8, 34.8, 30.3, 27.4, 18.7, 12.5, 6.3, 2.8, -2.6, -10.0, -16.7, -19.6, -20.2, -26.1, -34.5, -39.2, -35.2, -32.4, -38.8, -46.9, -52.2, -52.7, -58.1, -64.8, -69.3, -68.8, -68.8, -68.4, -67.9, -63.7, -59.2, -55.5, -51.2, -49.2, -44.9, -41.8, -37.4, -32.8, -28.3, -22.6, -18.4, -14.2, -10.6, -5.0, 3.1, 9.8, 15.6, 19.4, 26.4, 34.9, 42.5, 44.5, 39.8, 36.3, 37.7, 44.0, 49.4]


DTWVec = [0.3, -3.9, -4.2, -5.0, -5.4, -4.3, -2.5, -3.4, -5.3, -5.8, -5.8, -7.0, -8.8, -9.0, -6.7, -3.3, -0.2, 2.5, 3.6, 5.2, 8.4, 10.8, 13.4, 20.3, 28.5, 33.6, 40.4, 48.3, 54.8, 59.1, 64.9, 71.0, 74.7, 77.7, 82.0, 85.5, 87.2, 92.0, 94.5, 94.8, 95.5, 96.7, 95.9, 93.2, 91.5, 87.4, 83.0, 79.5, 74.3, 66.7, 62.5, 57.7, 48.2, 38.1, 32.8, 25.7, 16.6, 10.6, 5.3, -0.3, -5.3, -10.0, -15.9, -20.7, -23.9, -29.6, -36.4, -38.7, -36.9, -39.6, -44.5, -47.9, -48.7, -52.9, -56.0, -56.3, -56.5, -57.2, -56.7, -54.3, -51.6, -50.4, -47.0, -41.5, -37.6, -33.9, -27.6, -20.8, -16.4, -11.4, -4.8, 0.6, 6.2, 14.9, 22.6, 26.8, 29.0, 33.7, 40.8, 44.3]

pathArr = np.zeros((len(DTWVec), len(TestInput1)), dtype=np.float32)

def CalcDTWValues(pathArr, DTWVec, TestInput1):
    for i in range(len(DTWVec)):
        for j in range(len(TestInput1)):
            pathArr[i][j] = (DTWVec[i] - TestInput1[j])**2
    return pathArr

def CalcDTWValuesOpt(pathArr, DTWVec, TestInput1):
    f = len(TestInput1) / len(DTWVec)
    for i in range(len(DTWVec)):
        for j in range(len(TestInput1)):
            fi = f*i
            if j > fi-20 and j < fi+20:
                pathArr[i][j] = (DTWVec[i] - TestInput1[j])**2
    return pathArr

start = time.time()
pathArr = CalcDTWValues(pathArr, DTWVec, TestInput1)
end = time.time()
print("unoptimized", end-start)

pathArr.fill(100**2)

start = time.time()
pathArr = CalcDTWValuesOpt(pathArr, DTWVec, TestInput1)
end = time.time()
print("optimized", end-start)

def checkMin(pathArr, i, j):

    if pathArr[i[0]][j[0]] <= pathArr[i[1]][j[1]] and pathArr[i[0]][j[0]] <= pathArr[i[2]][j[2]]:
        return i[0],j[0]

    if pathArr[i[1]][j[1]] <= pathArr[i[0]][j[0]] and pathArr[i[1]][j[1]] <= pathArr[i[2]][j[2]]:
        return i[1],j[1]

    if pathArr[i[2]][j[2]] <= pathArr[i[1]][j[1]] and pathArr[i[2]][j[2]] <= pathArr[i[0]][j[0]]:
        return i[2],j[2]


def AccumulatedCostMatrix(pathArr, leni, lenj):
    dtw = pathArr.copy()
    dtw.fill(1000000)
    dtw[0,0] = 0
    n = leni
    m = lenj

    for i in range(1,n):
        if pathArr[i,0] < 10000:
            dtw[i,0] = dtw[i-1,0]+pathArr[i,0]
    for j in range(1,m):
        if pathArr[0,j] < 10000:
            dtw[0,j] = dtw[0,j-1]+pathArr[0,j]
    for i in range(1,n):
        for j in range(1,m):
            if pathArr[i,j] < 10000:
                dtw[i,j] = pathArr[i,j] + dtw[checkMin(dtw,[i, i-1, i-1], [j-1, j-1, j])]
    return dtw

def FindWarpingPathBackwards(Origin, dtw, leni, lenj):
    PathListX = []
    PathListY = []
    ValList = []
    PathLength = 0

    i = leni; j = lenj
    while i > 1 or j > 1:
        PathListX.insert(0, j)
        PathListY.insert(0, i)
        ValList.insert(0, Origin[i,j])
        PathLength += Origin[i,j]
        if i >= 1 and j >= 1:
            i, j = checkMin(dtw, [i, i-1, i-1], [j-1, j-1, j])
        elif i == 0:
            j -= 1
        elif j == 0:
            i -= 1
        else:
            continue
    return PathListX, PathListY, ValList, PathLength

dtw = AccumulatedCostMatrix(pathArr, len(DTWVec)-1, len(TestInput1)-1)
PX, PY, LV, PL = FindWarpingPathBackwards(pathArr, dtw, len(DTWVec)-1, len(TestInput1)-1)
PXR, PYR, LVR, PLR = FindWarpingPathBackwards(pathArr, pathArr, len(DTWVec)-1, len(TestInput1)-1)

idxi = 0
idxj = 0
GraphListX = []
GraphListY = []
ValueList = []
lastIdxDtw = len(DTWVec)-1
lastIdxInput = len(TestInput1)-1
sumVals = 0
rawPath = pathArr.copy()
while idxi < lastIdxDtw or idxj < lastIdxInput:
    GraphListY.append(idxi)
    GraphListX.append(idxj)
    ValueList.append(pathArr[idxi][idxj])
    sumVals += pathArr[idxi][idxj]
    pathArr[idxi][idxj] = 0
    if idxi < lastIdxDtw and idxj < lastIdxInput:
        idxi, idxj = checkMin(pathArr, [idxi, idxi+1, idxi+1], [idxj+1, idxj+1, idxj])
    elif idxi == lastIdxDtw:
        idxj += 1
    elif idxj == lastIdxInput:
        idxi += 1
    else:
        continue
pathArr[lastIdxDtw][lastIdxInput] = 0
GraphListY.append(idxi)
GraphListX.append(idxj)

for ix, iy in np.ndindex(pathArr.shape):
    if pathArr[ix][iy] != 0:
        pathArr[ix][iy] = 1

# create discrete colormap
#cmap = colors.ListedColormap(['red', 'tomato', 'orange','gold','yellow', 'yellowgreen', 'green', 'turquoise', 'blue','pink', 'purple'])
cmap = colors.ListedColormap(['#FFFFFF'])#,'#BBBBBB', '#777777','#333333','#111111'])
#bounds = [0,0.5,1,5,10,100,500,1000,5000,10000]
bounds = [0]#,5,100,500]
norm = colors.BoundaryNorm(bounds, cmap.N)

#plt.figure()
fig, ax = plt.subplots();
ax.imshow(rawPath, cmap=cmap, norm=norm)
ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=0.4)
ax.set_xticks(np.arange(0, len(GraphListX), 1));
ax.set_yticks(np.arange(0, len(GraphListY), 1));
#ax.plot(GraphListX, GraphListY, color='black')
ax.plot(PX, PY, color='gray')
#ax.plot(PXR, PYR, color='black')

fig, ax = plt.subplots();
ax.plot(ValueList, color="blue")
ax.plot(LV, color="green")
ax.plot(LVR, color="yellow")
print("pathLength", sumVals)
print("optimizedPathLengthCostOpti", PL)
print("optimizedPathLength", PLR)
plt.show()