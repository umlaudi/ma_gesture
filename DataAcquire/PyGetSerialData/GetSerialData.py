import serial
import time
from time import gmtime, strftime
import os

ser = serial.Serial('COM17', 115200, timeout=None)

startRecording = False
WriteToFile = False
DataList = []
DataNr = 1
Motion = "Negative"

dirPath = "../MotionData/" + Motion
try:
        os.mkdir(dirPath)
except OSError:
    print ("ne")
else:
    print ("Successfully created the directory %s " % dirPath)
dirPath = dirPath + "/RawData"
try:
        os.mkdir(dirPath)
except OSError:
    print("ne")
else:
    print ("Successfully created the directory %s " % dirPath)

while 1:
    try:
        mystring = str(ser.readline())
        mystring = mystring[2:-5]

        if 'Stop' in mystring:
            startRecording = False
            print(DataList)

            #write to file
            sTime = strftime("%Y_%m_%d %H_%M_%S", time.localtime())
            Filename = Motion + ' ' + sTime
            with open('../MotionData/' + Motion + '/RawData/' + Filename + '.csv', 'w') as f:
                f.write("x,y,z\n")
                for item in DataList:
                    f.write("%s\n" % item)
            DataList.clear()
            DataNr += 1
            print("wrote to file %s\n" % Filename)

        if startRecording:
            DataList.append(mystring)
        else:
            print(mystring)

        if 'Start' in mystring:
            startRecording = True

        #time.sleep(1)
    except ser.SerialTimeoutException:
        print('Data could not be read')
        time.sleep(1)