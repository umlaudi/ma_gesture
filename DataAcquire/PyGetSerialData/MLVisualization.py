import random
from matplotlib import pyplot as plt
import FileOps as fo
import numpy as np
import math
import matplotlib.gridspec as gridspec
from sklearn import svm, datasets
import matplotlib.image as mpimg
import preprocessing as pp

def LinearEq(x1, y1, x2, y2, x3=0, y3=0):
  if((x3 == 0 and y3 == 0) or (x3 != 0 and y3 != 0)):
    return 0.0
  yd = y1 - y2
  xd = x1 - x2
  k = yd / xd
  d = y1 - (k*x1)

  if(y3 == 0):
    return (x3*k) + d
  else:
    return (y3-d)/k

def make_meshgrid(x, y, h=.02):
  """Create a mesh of points to plot in

  Parameters
  ----------
  x: data to base x-axis meshgrid on
  y: data to base y-axis meshgrid on
  h: stepsize for meshgrid, optional

  Returns
  -------
  xx, yy : ndarray
  """
  x_min, x_max = x.min() - 1, x.max() + 1
  y_min, y_max = y.min() - 1, y.max() + 1
  xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                       np.arange(y_min, y_max, h))
  return xx, yy

def plot_contours(ax, clf, xx, yy, **params):
  """Plot the decision boundaries for a classifier.

  Parameters
  ----------
  ax: matplotlib axes object
  clf: a classifier
  xx: meshgrid ndarray
  yy: meshgrid ndarray
  params: dictionary of params to pass to contourf, optional
  """
  Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
  Z = Z.reshape(xx.shape)
  out = ax.contourf(xx, yy, Z, **params)
  return out

#HMM Visualization
Motion = "4" #which Motion
paths = ["../MotionData/" + Motion + "/normData"]
files, lMotionData = fo.getFilesFromFolder(paths)
picture = mpimg.imread("C:/Users/umlau/Documents/FH\MA/MasterArbeit/hgb-thesis-utf-20151104/Bilder/BspHmm4Emissions.png")
#lMotionPlt = [-1 * i for i in lMotionPlt]
#lMotionPlt = lMotionPlt[::-1]
num=4
lMotionPlt = lMotionData[num]['x'].values.tolist(); lMotionPlt = [i / 100 for i in lMotionPlt]
lMotionPlt2= lMotionData[num]['z'].values.tolist(); lMotionPlt2 = [i / 100 for i in lMotionPlt2]

lPitch = pp.CalcPitch(lMotionPlt,5)
lPitch2 = pp.CalcPitch(lMotionPlt2,5)
lDirs = pp.ClassifyPitchOpt(lMotionPlt, lPitch);  #lDirs = [i * 100 for i in lDirs]
lDirs2 = pp.ClassifyPitchOpt(lMotionPlt2, lPitch2);  #lDirs2 = [i * 100 for i in lDirs2]

XVal = []
for i in range(50):
  XVal.append(i)
YVal = [-110,110]
S0 = [9,9]
S1 = [20,20]
S2 = [32,32]
Tw = 1.5
font = 15

gs = gridspec.GridSpec(2,3)
ax1 = plt.subplot(gs[0:, 0]);plt.axis('off')
plt.imshow(picture, aspect='auto')
ax2 = plt.subplot(gs[0, 1:])
plt.ylim((-1.1,1.1)); plt.xlim((0,49))
plt.plot(lMotionPlt)
plt.plot(XVal,lDirs, 'k--', linewidth=0.6)
plt.plot(S0, YVal, 'k-', linewidth=Tw);plt.plot(S1, YVal, 'k-', linewidth=Tw);plt.plot(S2, YVal, 'k-', linewidth=Tw)
plt.text(0+1, 0.9, "0",fontsize=font);plt.text(S0[0]+1, 0.9, "1",fontsize=font);plt.text(S1[0]+1, 0.9, "2",fontsize=font);plt.text(S2[0]+1, 0.9, "3",fontsize=font);
ax3 = plt.subplot(gs[1, 1:])
plt.ylim((-1.1,1.1)); plt.xlim((0,49))
plt.plot(lMotionPlt2)
plt.plot(XVal, lDirs2, 'k--', linewidth=0.6)
plt.plot(S0, YVal, 'k-', linewidth=Tw);plt.plot(S1, YVal, 'k-', linewidth=Tw);plt.plot(S2, YVal, 'k-', linewidth=Tw)
plt.text(0+1, 0.9, "0",fontsize=font);plt.text(S0[0]+1, 0.9, "1",fontsize=font);plt.text(S1[0]+1, 0.9, "2",fontsize=font);plt.text(S2[0]+1, 0.9, "3",fontsize=font);
plt.show()

# import some data to play with
iris = datasets.load_iris()
# Take the first two features. We could avoid this by using a two-dim dataset
X = iris.data[:, :2]
#y = iris.target

#ML Visualization

X1 = []
X2 = []
Y1 = []
Y2 = []
X3 =[]
Y3 = []
XL = []
YL = []
XLRbf =[]
YLRbf = []
XL3 = []
YL3 = []
"""
for x in range(30):
  XL.append([random.randint(1500,3000)/1000,random.randint(3000,5700)/1000])
  YL.append(1)
  Y1.append(XL[-1][1])
  X1.append(XL[-1][0])
  XL.append([random.randint(3000,5500)/1000,random.randint(1000,2300)/1000])
  YL.append(2)
  Y2.append(XL[-1][1])
  X2.append(XL[-1][0])
  XL3.append(XL[-2]); YL3.append(1)
  XL3.append(XL[-1]); YL3.append(2)
  XL3.append([random.randint(4000,5000)/1000,random.randint(4000,5000)/1000]);YL3.append(3)
  Y3.append(XL3[-1][1])
  X3.append(XL3[-1][0])

  phi = random.randint(0,3600)/10
  r = random.randint(0,1000)/1000
  x = 3+ r*math.cos(phi)
  y = 3+ r*math.sin(phi)
  XLRbf.append([x,y])
  YLRbf.append(1)
  phi = random.randint(0,3600)/10
  r = random.randint(1100,2100)/1000
  x = 3+ r*math.cos(phi)
  y = 3+ r*math.sin(phi)
  XLRbf.append([x,y])
  YLRbf.append(-1)

Y = np.asarray(YL)
X = np.asarray(XL)
XSvm = np.asarray(XL3)
YSvm = np.asarray(YL3)
YRbf = np.asarray(YLRbf)
XRbf = np.asarray(XLRbf)

model = svm.SVC(kernel="linear", C=10.0)
model.fit(X,Y)

# get the separating hyperplane
w = model.coef_[0]
a = -w[0] / w[1]
xx = np.linspace(-5, 5)
yy = a * xx - (model.intercept_[0]) / w[1]

margin = 1 / np.sqrt(np.sum(model.coef_ ** 2))
yy_down = yy - np.sqrt(1 + a ** 2) * margin
yy_up = yy + np.sqrt(1 + a ** 2) * margin

# plot the line, the points, and the nearest vectors to the plane
#plt.figure(fignum, figsize=(4, 3))
#plt.clf()
plt.xlim(0, 6)
plt.ylim(0, 6)

plt.plot(xx, yy, 'k-')
plt.plot(xx, yy_down, 'k--')
plt.plot(xx, yy_up, 'k--')

plt.scatter(model.support_vectors_[:, 0], model.support_vectors_[:, 1], s=80,
            facecolors='none', zorder=10, edgecolors='k')
#plt.scatter(X[:, 0], X[:, 1], c=Y, zorder=10, cmap=plt.cm.Paired,
#            edgecolors='k')
plt.scatter(X1,Y1, marker='d', s=30)
plt.scatter(X2,Y2, marker='.', s=30)
plt.scatter(X3,Y3, marker='*', s=30)
plt.xlabel("F1")
plt.ylabel("F2")

#plt.show()


C = 1.0  # SVM regularization parameter
models = (svm.SVC(kernel='linear', C=C),
          svm.LinearSVC(C=C),
          svm.SVC(kernel='rbf', gamma=0.7, C=C),
          svm.SVC(kernel='poly', degree=3, C=C))
#models = (clf.fit(X, Y) for clf in models)
models[0].fit(XSvm,YSvm)
models[1].fit(XSvm,YSvm)
models[2].fit(XRbf,YRbf)
models[3].fit(XSvm,YSvm)

# title for the plots
titles = ('linear Kernel (one vs. one)',
          'linear Kernal (one vs. all)',
          'RBF kernel',
          'polynomial (degree 3) kernel')

# Set-up 2x2 grid for plotting.
fig, sub = plt.subplots(2, 2)
plt.subplots_adjust(wspace=0.4, hspace=0.4)

X0, X1 = XSvm[:, 0], XSvm[:, 1]
xx, yy = make_meshgrid(X0, X1)

i=0
for clf, title, ax in zip(models, titles, sub.flatten()):
    if i == 2:
      X0,X1 = XRbf[:,0], XRbf[:,1]
      littlec = YRbf
    else:
      X0, X1 = XSvm[:, 0], XSvm[:, 1]
      littlec = YSvm

    i += 1

    plot_contours(ax, clf, xx, yy,
                   alpha=0.8)
    ax.scatter(X0, X1, c=littlec,  s=20, edgecolors='k')
    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())
    ax.set_xticks(())
    ax.set_yticks(())
    ax.set_title(title)

plt.show()
"""



#X0, X1 = X[:, 0], X[:, 1]
#xx, yy = make_meshgrid(X0, X1)
#
#plt.scatter(X0, X1)
#plt.set_xlim(xx.min(), xx.max())
#plt.set_ylim(yy.min(), yy.max())
"""
plt.subplot(121)
plt.ylim((0, 7))
plt.xlim((0, 7))
plt.scatter(X1, Y1, marker='d')
plt.scatter(X2, Y2, marker='.')
y1 = 0.5; y2  = 6.5
x1 = LinearEq(3.15,3,2.8,1.8, y3=y1);x2 = LinearEq(3.15,3,2.8,1.8, y3=y2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linewidth='1.5'))
plt.subplot(122)
plt.ylim((0, 7))
plt.xlim((0, 7))
plt.scatter(X1, Y1, marker='d')
plt.scatter(X2, Y2, marker='.')
x1 = 0.8; x2  = 6.2
y1 = LinearEq(0.7,1.9,5.4,3.6, x3=x1);y2 = LinearEq(0.7,1.9,5.4,3.6, x3=x2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linewidth='1.5'))
plt.show()
"""
#ML C Parameter
X1 = []
X2 = []
Y1 = []
Y2 = []
X=[]
Y=[]
for x in range(40):
  Y1.append(random.randint(3000,6000)/1000)
  X1.append(random.randint(1500,3000)/1000)
  Y2.append(random.randint(500,1700)/1000)
  X2.append(random.randint(3000,5500)/1000)

  XL.append([X1[-1],Y1[-1]])
  XL.append([X2[-1],Y2[-1]])
  YL.append(1)
  YL.append(-1)

Y1.extend([1.8, 1.3, 3])
X1.extend([2.6,2.3, 3])
Y2.extend([3,3.2, 1.7, 1.8])
X2.extend([3.3, 3.4, 3, 4.7])
XL.append([X1[-1], Y1[-1]])
XL.append([X2[-1], Y2[-1]])
YL.append(1)
YL.append(-1)
X = np.asarray(XL)
Y = np.asarray(YL)


models = (svm.SVC(kernel="linear", C=0.1),svm.SVC(kernel="linear", C=1000.0))
models[0].fit(X,Y)
models[1].fit(X,Y)
fig, axs = plt.subplots(1,2)
for model, axis in zip(models,axs):
  # get the separating hyperplane
  w = model.coef_[0]
  a = -w[0] / w[1]
  xx = np.linspace(-5, 5)
  yy = a * xx - (model.intercept_[0]) / w[1]

  margin = 1 / np.sqrt(np.sum(model.coef_ ** 2))
  yy_down = yy - np.sqrt(1 + a ** 2) * margin
  yy_up = yy + np.sqrt(1 + a ** 2) * margin

  # plot the line, the points, and the nearest vectors to the plane
  #plt.figure(fignum, figsize=(4, 3))
  #plt.clf()
  axis.set_xlim(0, 6)
  axis.set_ylim(0, 6)

  axis.plot(xx, yy, 'k-')
  axis.plot(xx, yy_down, 'k--')
  axis.plot(xx, yy_up, 'k--')

  axis.scatter(model.support_vectors_[:, 0], model.support_vectors_[:, 1], s=80,
            facecolors='none', zorder=10, edgecolors='k')
  #plt.scatter(X[:, 0], X[:, 1], c=Y, zorder=10, cmap=plt.cm.Paired,
  #            edgecolors='k')
  axis.scatter(X1,Y1, marker='d', s=30)
  axis.scatter(X2,Y2, marker='.', s=30)

plt.show()


plt.subplot(121)
plt.title("C = 10")
plt.ylim((0, 7))
plt.xlim((0, 7))
plt.scatter(X1, Y1, marker='d')
plt.scatter(X2, Y2, marker='.')
y1 = 0.5; y2  = 5.5
x1 = LinearEq(3.15,3,2.8,1.8, y3=y1);x2 = LinearEq(3.15,3,2.8,1.8, y3=y2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linewidth='1.5'))
x1 = LinearEq(3,3,2.65,1.8, y3=y1);x2 = LinearEq(3,3,2.65,1.8, y3=y2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linestyle='--', linewidth='1'))
x1 = LinearEq(3.3,3,2.95,1.8, y3=y1);x2 = LinearEq(3.3,3,2.95,1.8, y3=y2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linestyle='--', linewidth='1'))
plt.subplot(122)
plt.title("C = 0.1")
plt.ylim((0, 7))
plt.xlim((0, 7))
plt.scatter(X1, Y1, marker='d')
plt.scatter(X2, Y2, marker='.')
x1 = 0.5; x2  = 6.5
y1 = LinearEq(3,2.35,4.7,2.45, x3=x1);y2 = LinearEq(3,2.35,4.7,2.45, x3=x2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linewidth='1.5'))
y1 = LinearEq(3,3,4.7,3.1, x3=x1);y2 = LinearEq(3,3,4.7,3.1, x3=x2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linestyle='--', linewidth='1'))
y1 = LinearEq(3,1.7,4.7,1.8, x3=x1);y2 = LinearEq(3,1.7,4.7,1.8, x3=x2)
plt.annotate(text='', xy = (x1, y1), xytext=(x2,y2), arrowprops=dict(arrowstyle='-', linestyle='--', linewidth='1'))
plt.show()





#DTW Visualization
Motion = "kringel" #which Motion
paths = ["../MotionData/" + Motion + "/normData"]
files, lMotionData = fo.getFilesFromFolder(paths)
paths = ["../MotionData/" + Motion + "/DTWSeq"]
files, lDTWSeq = fo.getFilesFromFolder(paths)
lDTWPlt = lDTWSeq[0]['z'].values.tolist()
lDTWPlt = [-1 * i for i in lDTWPlt]
lMotionPlt = lMotionData[0]['x'].values.tolist()
#lMotionPlt = [-1 * i for i in lMotionPlt]
#lMotionPlt = lMotionPlt[::-1]

gs = gridspec.GridSpec(5,5)
ax1 = plt.subplot(gs[0, 2:])
plt.ylabel("DTW-Muster")
plt.xticks([]);plt.yticks([]);plt.box(False)
plt.ylim((-150, 62))
plt.plot(lDTWPlt)
plt.annotate(text='', xy=(len(lDTWPlt), -120), xytext=(0,-120), arrowprops=dict(arrowstyle='<->'))
plt.text((len(lDTWPlt)/2)-1,-160,"i")


ax2 = plt.subplot(gs[1, 2:]);plt.axis('off')
dataY = [["d[0,0]"],["+d[1,0]"],["..."],["+d[i,0]"]]
dataX = [["d[0,0]","+d[0,1]","...","+d[0,j]"]]
xtable = plt.table(cellText=dataX,  cellLoc='center',loc='center', colWidths=[0.25,0.25,0.25,0.25])
xtable.set_fontsize(16)
xtable.scale(1,2.6)

ax3 = plt.subplot(gs[2:, 0])
lMotionPlt = lMotionPlt[::-1]
xData = []
for i in range(len(lMotionPlt)):
  xData.append(i)
plt.xlabel("Bsp Geste")
plt.xticks([]);plt.yticks([]);plt.box(False)
plt.xlim((62, -150))
plt.annotate(text='', xy=(-120,len(lMotionPlt)), xytext=(-120,-1), arrowprops=dict(arrowstyle='<->'))
plt.text(-130,(len(lMotionPlt)/2)-1,"j")
plt.plot(lMotionPlt,xData)

ax4 = plt.subplot(gs[2:, 1]);plt.axis('off')
ytable = plt.table(cellText=dataY, cellLoc='center', loc='center', colWidths=[0.4])
ytable.set_fontsize(16)
ytable.scale(2,3)

ax5 = plt.subplot(gs[2:, 2:]);plt.axis('off')
dataXY = [["d00","d01","...","d0j"],["d10","o+d[1,1]","...","..."],["...","...","...","o+d[2,3]"],["di0","...","...","..."]]
ytable = plt.table(cellText=dataXY,  cellLoc='center',loc='center', colWidths=[0.25,0.25,0.25,0.25])
ytable.set_fontsize(14)
ytable.scale(1,3)

plt.show()

gs = gridspec.GridSpec(4, 2)
ax1 = plt.subplot(gs[0, 0]);
plt.ylabel("DTW-Muster")
plt.xticks([])
plt.yticks([])
plt.box(False)
plt.ylim((-150, 62))
plt.plot(lDTWPlt)
#plt.arrow(0,-120, len(lDTWPlt)-1,0,length_includes_head=True, head_width=10, head_length=1.5, gid="helno", fc='k', ec='k')
plt.annotate(text='', xy=(len(lDTWPlt), -120), xytext=(0,-120), arrowprops=dict(arrowstyle='<->'))
plt.text((len(lMotionPlt)/2)-1,-160,"i")
ax2 = plt.subplot(gs[1, 0]);
plt.ylabel("Bsp Geste")
plt.xticks([])
plt.yticks([])
plt.box(False)
plt.ylim((-150, 62))
#plt.arrow(0,-120, len(lMotionPlt)-1,0, label='henlo', arrowprops=dict(arrowstyle='<->'))
plt.annotate(text='', xy=(len(lMotionPlt), -120), xytext=(0,-120), arrowprops=dict(arrowstyle='<->'))
plt.text((len(lMotionPlt)/2)-1,-160,"j")
plt.plot(lMotionPlt)


ax3 = plt.subplot(gs[:2, 1]);plt.axis('off')
data = [["d(0,0)","+d(0,1)","...","+d(0,j)"],["","","",""],["","","",""],["","","",""]]
xlbl1 = ['d(0,0)', '+d(0,1)', '...', '+d(0,j)']
ylbl1 = ['d(0,0)', '+d(1,0)', '...', '+d(i,0)']
clust_data = np.zeros((4,4))
ytable = plt.table(cellText=data, loc='center')
ytable.scale(1,2.6)

ax4 = plt.subplot(gs[2:, 0]);plt.axis('off')
data = [["d(0,0)","","",""],["+d(1,0)","","",""],["...","","",""],["+d(i,0)","","",""]]
ytable = plt.table(cellText=data, loc='center', colWidths=[0.25,0.25,0.25,0.25])
ytable.scale(1,2.6)
ax5 = plt.subplot(gs[2:, 1]);plt.axis('off')
ytable = plt.table(cellText=data, loc='center', colWidths=[0.25,0.25,0.25,0.25])
ytable.scale(1,2.6)
plt.show()