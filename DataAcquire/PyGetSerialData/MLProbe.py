import pandas as pd
from pandas import DataFrame, Series, Timedelta, concat
from matplotlib import pyplot as plt
import os
import preprocessing as pp
import FileOps as fo
import numpy as np
import statistics
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from collections import defaultdict
from sklearn.metrics import confusion_matrix
from pandas.plotting import scatter_matrix



nix, X_Test1 = fo.getFilesFromFolder(["../MotionData/1/normData"]);Y_Test1 = ['1'] * len(X_Test1)
nix, X_Test3 = fo.getFilesFromFolder(["../MotionData/3/normData"]);Y_Test3 = ['3'] * len(X_Test3)
nix, X_TestKr = fo.getFilesFromFolder(["../MotionData/kringel/normData"]);Y_TestKr = ['kringel'] * len(X_TestKr)
nix, X_Test4 = fo.getFilesFromFolder(["../MotionData/4/normData"]);Y_Test4 = ['4'] * len(X_Test4)
nix, X_Test6 = fo.getFilesFromFolder(["../MotionData/6/normData"]);Y_Test6 = ['6'] * len(X_Test6)
X_TestR = []
Y_TestR = []
X_TestR.append(X_Test1.copy()); Y_TestR.extend(Y_Test1)
X_TestR.append(X_Test3.copy()); Y_TestR.extend(Y_Test3)
X_TestR.append(X_TestKr.copy()); Y_TestR.extend(Y_TestKr)
X_TestR.append(X_Test4.copy()); Y_TestR.extend(Y_Test4)
X_TestR.append(X_Test6.copy()); Y_TestR.extend(Y_Test6)
X_Test = []
MotionNames = ["3","kringel", "1", "4","6"]
XScat = []
for j in range(len(X_TestR)):
    for i in range(len(X_TestR[j])):
        classList = [j] * len(X_TestR[j][i])
        bufferScat = X_TestR[j][i].drop(columns=['y'])
        DataIloc = bufferScat.iloc[25]
        DataIloc['target'] = int(j)
        DataIloc['class'] = MotionNames[j]
        XScat.append(DataIloc.values)
names = ['x', 'z', 'target', 'class']
df = DataFrame(
    data = XScat,
    columns = names
)

print(df.loc[:])

scatter_matrix(df, c=df.target)

X=df[['x','z']]

# make a grid
x1 = np.linspace(1,7,61)
x2 = np.linspace(1,7,61)
X1,X2 = np.meshgrid(x1,x2)
x_grid = np.array(list(zip(X1.ravel(), X2.ravel())))

Y_bin = (df.target == 2)
scatter_matrix(X, c=Y_bin);
plt.show()