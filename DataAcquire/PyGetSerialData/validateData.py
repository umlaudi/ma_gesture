import pandas as pd
from matplotlib import pyplot as plt
import os
import preprocessing as pp
import FileOps as fo

Motion = "1" #which Motion should be validated
paths = ["../MotionData/" + Motion + "/RawData"]

#read raw Motion values
files, lMotionData = fo.getFilesFromFolder(paths)


i = 0
while i < len(lMotionData):
    #fig, axes = plt.subplots(nrows=2, ncols=2)
    if i < len(lMotionData):
        plt.subplot(221).set_title(files[i])
        plt.plot(lMotionData[i]["x"], '-', linewidth=1)
        plt.plot(lMotionData[i]["y"], '-', linewidth=1)
        plt.plot(lMotionData[i]["z"], '-', linewidth=1)
        plt.legend(loc="upper left")
    if i+1 < len(lMotionData):
        plt.subplot(222).set_title(files[i+1])
        plt.plot(lMotionData[i+1], '-', linewidth=1)
    if i+2 < len(lMotionData):
        plt.subplot(223).set_title(files[i+2])
        plt.plot(lMotionData[i+2], '-', linewidth=1)
    if i+3 < len(lMotionData):
        plt.subplot(224).set_title(files[i+3])
        plt.plot(lMotionData[i+3], '-', linewidth=1)
    plt.show()
    i +=4
print("hello")