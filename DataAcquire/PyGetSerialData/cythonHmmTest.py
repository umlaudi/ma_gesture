import numpy as np
import matplotlib.pyplot as plt
import hmms


Pi = np.array([0.6, 0.2,0.1,0.1])

A = np.array([[0.3, 0.6,0.05,0.05],
    [0.05, 0.35, 0.55, 0.05],
    [0.05, 0.55, 0.35, 0.05],
    [0.45, 0.05, 0.05, 0.45]])

B = np.array([[0.3, 0.6,0.05,0.05],
    [0.05, 0.35, 0.55, 0.05],
    [0.05, 0.55, 0.35, 0.05],
    [0.45, 0.05, 0.05, 0.45]])

S1n = np.array([0,0,1,1,1,2,2,2,2,2,3,3,3,0,0])
K1n = np.array([0,0,1,1,1,2,2,2,2,2,3,3,3,0,0])
K1 = np.array([0,1,1,1,2,2,2,3,3,3,0,0,0,0])
K2 = np.array([0,0,0,3,3,3,2,2,2,2,1,1,1,0])
K1t = [0,0,1,1,1,2,2,2,2,2,3,3,3,0,0]
K1tV = np.array([K1n,K1n])

dhmm = hmms.DtHMM(A, B, Pi)
hmms.print_parameters(dhmm)

print(np.exp(dhmm.emission_estimate(K1n)))
print(np.exp(dhmm.estimate(S1n, K1n)))

dhmm.maximum_likelihood_estimation(S1n, K1n)
dhmm.baum_welch(K1tV, 10)
hmms.print_parameters(dhmm)

print(np.exp(dhmm.emission_estimate(K1n)))
print(np.exp(dhmm.emission_estimate(K1)))
print(np.exp(dhmm.emission_estimate(K2)))