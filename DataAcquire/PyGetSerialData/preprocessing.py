#feature extraction
def CalcPitch(fData, mask):
    maskFirstHalf = int(((mask-1)/2))
    pData = [0]*maskFirstHalf
    avgX = 0; avgY=0; za = 0; ne= 0
    for i in range(len(fData)):
        if i <= len(fData) - mask:
            for j in range(mask):
                avgX += i+j
                avgY += fData[i+j]
            avgY = avgY / mask
            avgX = avgX / mask
            for j in range(mask):
                za += ((i+j)-avgX)*(fData[i+j]-avgY)
                ne += ((i+j)-avgX)**2
            pData.append(za/ne)
            za=0; ne=0; avgX=0; avgY=0
    for i in range(maskFirstHalf):
        pData.append(0)
    return pData

def hysteresis(val, lastVal, maxVal, hist):
    if val >= maxVal/5:
        if lastVal == 1:
            return 1
        elif lastVal == 0 and val >= (maxVal/3) + hist:
            return 1
        else:
            return 0
    else:
        if lastVal == 0:
            return 0
        elif lastVal == 1 and val <= (maxVal/3) - hist:
            return 0
        else:
            return 1

def ClassifyPitchOpt(fData, pitchD):
    pitchClass = []
    maxv = max(fData); hystvp = maxv/3
    minv = min(fData); hystvn = minv/3
    maxp = max(pitchD); hystpp = maxp/2
    minp = min(pitchD); hystpn = minp/2
    LastClass = 0
    for i in range(len(fData)):
        if fData[i] >= hystvp:
            pitchClass.append(1)
        elif fData[i] <= hystvn:
            pitchClass.append(-1)
        else:
            if LastClass == -1 and pitchD[i] > hystpp:
                pitchClass.append(-1)
            elif LastClass == 1 and pitchD < hystpn:
                pitchClass.append(1)
            else:
                pitchClass.append(0)
    return pitchClass

def ClassifyPitch(pitchD):
    pitchClass = []
    maxv = max(pitchD)
    minv = min(pitchD)
    lastP = 0
    lastN = 0
    for i in range(len(pitchD)):
        if pitchD[i] > 0:
            buf = hysteresis(pitchD[i], lastP, abs(maxv), abs(maxv/6))
            pitchClass.append(buf)
            lastP = buf

            #if pitchD[i] >= maxv/2:
            #    pitchClass.append(1)
            #else:
            #    pitchClass.append(0)
        else:
            buf = hysteresis(abs(pitchD[i]), lastN, abs(minv), abs(minv / 6))
            lastN = buf
            buf = buf*-1
            pitchClass.append(buf)
            #if pitchD[i] <= minv/2:
            #    pitchClass.append(-1)
            #else:
            #    pitchClass.append(0)
    return pitchClass

def OffsetCorr(RawData):
    Offset = (RawData[0] + RawData[1] + RawData[2])/3
    oData = []
    for i in range(len(RawData)):
        oData.append(RawData[i]-Offset)
    return oData

#average filter function
def MovingAverage(RawData, mask):
    fData = []
    mysum = 0
    maskFirstHalf = int(((mask-1)/2))
    for i in range(len(RawData)):
        #if i > 1 and i < len(RawData)-2:
        #    fData.append((RawData[i-2]+RawData[i-1] + RawData[i] + RawData[i+1] + RawData[i-2])/5)
        if i <= len(RawData)-mask:
            for j in range(mask):
                mysum += RawData[i+j]
            fData.append(mysum/mask); mysum = 0
    for i in range(maskFirstHalf):
        fData.insert(0,0)
        fData.append(0)

    return fData

#interpolate function
def interpolate(inp, fi):
    i = int(fi)
    f = fi - i
    return (inp[i] if f < 0.05 else
            inp[i] + f*(inp[i+1]-inp[i]))

# normalize amplitude
def Normalize(fData, newMaxVal):
    maxv = max(fData)
    minv = min(fData)
    if abs(minv) > maxv:
        maxv = abs(minv)
    fData[:] = [((x / maxv) * newMaxVal) for x in fData]
    return fData

def ClassifyDir(x, z):
    if x == 0:
        if z == 0:
            return 0
        elif z == 1:
            return 1
        elif z == -1:
            return 5
    elif x == 1:
        if z == 0:
            return 3
        elif z == 1:
            return 2
        elif z == -1:
            return 4
    elif x == -1:
        if z == 0:
            return 7
        elif z == 1:
            return 8
        elif z == -1:
            return 6