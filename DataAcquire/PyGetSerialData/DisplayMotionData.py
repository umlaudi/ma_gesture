import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

FilePath1 = "../MotionData/Test1.csv"
FilePath2 = "../MotionData/Test2.csv"
FilePath3 = "../MotionData/Test3.csv"
FilePath4 = "../MotionData/Test4.csv"

Motions = [pd.read_csv(FilePath1),pd.read_csv(FilePath2),pd.read_csv(FilePath3),pd.read_csv(FilePath4)]
Motions2 = Motions.copy()
#print(Motions.head())
#maxX = max(Motions[0]['x'])
dfList = []
dfListAvg = []
dfListFin = []
avgX = []; avgY = []; avgZ = []
avgXYZ = []
avgMotions = []
normX = []; normY = []; normZ = []
#globally defined motion vector length
gLen = 100

#feature extraction
def CalcPitch(fData, mask):
    pData = [0]
    avgX = 0; avgY=0; za = 0; ne= 0
    for i in range(len(fData)):
        if i < len(fData) - mask:
            for j in range(mask):
                avgX += i+j
                avgY += fData[i+j]
            avgY = avgY / mask
            avgX = avgX / mask
            for j in range(mask):
                za += ((i+j)-avgX)*(fData[i+j]-avgY)
                ne += ((i+j)-avgX)**2
            pData.append(za/ne)
            za=0; ne=0; avgX=0; avgY=0
    pData.append(0)
    pData.append(0)
    pData.append(0)
    return pData

def hysteresis(val, lastVal, maxVal, hist):
    if val >= maxVal/2:
        if lastVal == 1:
            return 1
        elif lastVal == 0 and val >= (maxVal/2) + hist:
            return 1
        else:
            return 0
    else:
        if lastVal == 0:
            return 0
        elif lastVal == 1 and val <= (maxVal/2) - hist:
            return 0
        else:
            return 1


def ClassifyPitch(pitchD):
    pitchClass = []
    maxv = max(pitchD)
    minv = min(pitchD)
    lastP = 0
    lastN = 0
    for i in range(len(pitchD)):
        if pitchD[i] > 0:
            buf = hysteresis(pitchD[i], lastP, abs(maxv), abs(maxv/5))
            pitchClass.append(buf)
            lastP = buf

            #if pitchD[i] >= maxv/2:
            #    pitchClass.append(1)
            #else:
            #    pitchClass.append(0)
        else:
            buf = hysteresis(abs(pitchD[i]), lastN, abs(minv), abs(minv / 5))
            lastN = buf
            buf = buf*-1
            pitchClass.append(buf)
            #if pitchD[i] <= minv/2:
            #    pitchClass.append(-1)
            #else:
            #    pitchClass.append(0)
    return pitchClass

#average filter function
def MovingAverage(RawData):
    fData = []
    for i in range(len(RawData)):
        if i > 0 and i < len(RawData)-1:
            fData.append((RawData[i-1] + RawData[i] + RawData[i+1])/3)
    return fData

#interpolate function
def interpolate(inp, fi):
    i = int(fi)
    f = fi - i
    return (inp[i] if f < 0.05 else
            inp[i] + f*(inp[i+1]-inp[i]))

# normalize amplitude
def Normalize(fData, newMaxVal):
    maxv = max(fData);
    fData[:] = [((x / maxv) * newMaxVal) for x in fData]
    return fData

####################################
TestString = Motions[1]['y']
TestString = MovingAverage(TestString)
TestString = Normalize(TestString, 100)
####################################



#apply average filter
for i in range(len(Motions)):
    avgX = MovingAverage(Motions[i]['x'])
    avgY = MovingAverage(Motions[i]['y'])
    avgZ = MovingAverage(Motions[i]['z'])
    #for j in range(len(Motions[i]['x'])):
        #if j > 0 and j < len(Motions[i]['x'])-1:
            #avgX.append((Motions[i]['x'][j-1] + Motions[i]['x'][j] + Motions[i]['x'][j+1])/3)
            #avgY.append((Motions[i]['y'][j-1] + Motions[i]['y'][j] + Motions[i]['y'][j+1])/3)
            #avgZ.append((Motions[i]['z'][j-1] + Motions[i]['z'][j] + Motions[i]['z'][j+1])/3)

    # normiere die Länge
    deltaLen = (len(avgX)-1) / float(gLen-1)
    normX = [interpolate(avgX, i*deltaLen) for i in range(gLen)]
    normY = [interpolate(avgY, i*deltaLen) for i in range(gLen)]
    normZ = [interpolate(avgZ, i*deltaLen) for i in range(gLen)]

    # normiere die amplitude
    #maxv = max(normX); normX[:] = [((x/maxv)*100) for x in normX]
    #maxv = max(normY); normY[:] = [((x/maxv)*100) for x in normY]
    #maxv = max(normZ); normZ[:] = [((x/maxv)*100) for x in normZ]
    normX = Normalize(normX, 100)
    normY = Normalize(normY, 100)
    normZ = Normalize(normZ, 100)

    avgXYZ.append(normX.copy()); avgX.clear(); normX.clear()
    avgXYZ.append(normY.copy()); avgY.clear(); normY.clear()
    avgXYZ.append(normZ.copy()); avgZ.clear(); normZ.clear()
    avgMotions.append(avgXYZ.copy())
    avgXYZ.clear()
#    Motions[i]['x'] = Motions[i]['x']/max(Motions[i]['x'])
#    Motions[i]['y'] = Motions[i]['y']/max(Motions[i]['y'])
#    Motions[i]['z'] = Motions[i]['z']/max(Motions[i]['z'])

#make data frames
for i in range(len(avgMotions)):
    df = pd.DataFrame(
        data = {
            'X' : avgMotions[i][0],
            'Y' : avgMotions[i][1],
            'Z' : avgMotions[i][2]
        }
    )
    dfListAvg.append(df)

fig, axes = plt.subplots(nrows=2, ncols=2)
#plt.subplot(221);
dfListAvg[0].plot(ax=axes[0,0], grid=True)
#plt.subplot(222);
dfListAvg[1].plot(ax=axes[0,1], grid=True)
#plt.subplot(223);
dfListAvg[2].plot(ax=axes[1,0], grid=True)
#plt.subplot(224);
dfListAvg[3].plot(ax=axes[1,1], grid=True)
#plt.show()

for i in range(len(Motions)):
    df = pd.DataFrame(
        data = {
            'X' : Motions[i]['x'],
            'Y' : Motions[i]['y'],
            'Z' : Motions[i]['z']
        }
    )
    dfList.append(df)

fig, axes = plt.subplots(nrows=2, ncols=2)
#plt.subplot(221);
dfList[0].plot(ax=axes[0,0], grid=True)
#plt.subplot(222);
dfList[1].plot(ax=axes[0,1], grid=True)
#plt.subplot(223);
dfList[2].plot(ax=axes[1,0], grid=True)
#plt.subplot(224);
dfList[3].plot(ax=axes[1,1], grid=True)
#plt.show()

DTWList = []
for j in range(len(avgMotions[0][1])):
    DTWList.append((avgMotions[0][1][j] + avgMotions[1][1][j] + avgMotions[2][1][j] + avgMotions[3][1][j])/4)

df = pd.DataFrame(
        data = {
            'X1' : avgMotions[0][1],
            'X2' : avgMotions[1][1],
            'X3' : avgMotions[2][1],
            'X4' : avgMotions[3][1]
            #'DTW' : DTWList
        }
    )
dfListFin.append(df)
df = pd.DataFrame(
        data = {
            'Z1' : avgMotions[0][2],
            'Z2' : avgMotions[1][2],
            'Z3' : avgMotions[2][2],
            'Z4' : avgMotions[3][2]
        }
    )
dfListFin.append(df)

fig, axes = plt.subplots(nrows=2, ncols=2)
plt.subplot(221)
plt.plot(dfListFin[0], '.', linewidth=0.1)
plt.plot(DTWList, '-', linewidth=2)
plt.subplot(222); plt.plot(dfListFin[1])
pitches3 = CalcPitch(DTWList, 3)
pitches5 = CalcPitch(DTWList, 5)
pitches7 = CalcPitch(DTWList, 7)
plt.subplot(223);
plt.plot(pitches3, '.')
plt.plot(pitches5, '.')
plt.plot(pitches7, '.')
plt.subplot(224);
plt.legend("on")
plt.plot(ClassifyPitch(pitches3), 'r-')
plt.plot(ClassifyPitch(pitches5), 'b-')
plt.plot(ClassifyPitch(pitches7), 'g-')
plt.show()



print("TestInput1 = [", end='')
for i in range(len(avgMotions[0][0])):
    print("%.1f" % avgMotions[3][1][i], end='')
    if i < len(avgMotions[0][0])-1:
        print(", ", end='')
print("]\n")

print("DTWVec = [", end='')
for i in range(len(DTWList)):
    print("%.1f" % DTWList[i], end='')
    if i < len(DTWList)-1:
        print(", ", end='')
print("]\n")

print("TestString = [", end='')
for i in range(len(TestString)):
    print("%.1f" % TestString[i], end='')
    if i < len(TestString)-1:
        print(", ", end='')
print("]\n")