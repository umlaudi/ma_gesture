import numpy as np
import matplotlib.pyplot as plt
import hmms
import FileOps as fo
import random

MaxP = 1e-37

def ForwardAlgo(Pi, b, a, o):
    nS = len(a)
    nO = len(o)

    f = np.zeros((nO, nS))#[[0]*nS]*nO
    for i in range(nS):
        f[0,i] = Pi[i] * b[0][o[0]]
    for t in range(1, nO):
        for i in range(nS):
            sum = 0
            for j in range(nS):
                sum += f[t-1,j]*a[j][i]
            sum *= b[i][o[t]]
            f[t,i] = sum
    P = 0
    for j in range(nS):
        P += f[nO-1,j]
    return P

Motions = ["1","2","3","4","5","6","kringel","N", "Negative"]
HMMNames = ["1","2","3","4","5","6","Kr","N"]
directionData = []
dhmm = []
for i in range(len(Motions)):
    path = ["../MotionData/" + Motions[i] + "/directions"]
    files, lMotionData = fo.getFilesFromFolder(path)
    directionData.append(lMotionData)
for i in range(len(HMMNames)):
    ModelName = "HMMModels/FinalHMM" + HMMNames[i] + ".npz"
    dhmm.append(hmms.DtHMM.from_file(ModelName))

right = 0;wrong = 0
TP = 0;TN = 0;FP = 0;FN = 0
for i in range(len(directionData)):
    for j in range(len(directionData[i])):
        maxVal = 0; idx = 99
        for k in range(len(HMMNames)):
            directionSample = np.squeeze(np.array(directionData[i][j].values, dtype=np.int32))
            P = ForwardAlgo(dhmm[k].pi, dhmm[k].b, dhmm[k].a, directionSample)
            if P > MaxP and P > maxVal:
                maxVal = P
                idx = k

        if i != 8:
            if idx == i:
                right += 1
            else:
                wrong += 1

            if idx == 99:
                FN +=1
            else:
                TP +=1
        else:
            if idx == 99:
                TN +=1
            else:
                FP +=1


print("right: ", right, ", wrong: ", wrong)
print("TP: ", TP,", TN: ", TN,", FP: ", FP,", FN: ", FN)