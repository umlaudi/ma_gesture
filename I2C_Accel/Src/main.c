/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "accel_defines.h"
#include "stdio.h"
#include <string.h>

#define MAX_VEC_LEN 200
#define NEW_LEN 50
#define NORMALIZE_VAL 100
#define MASK_SIZE 5

//#define SEND_TO_MASTER
#define USE_PP
//#define USE_ML
#define USE_DTW
//#define USE_HMM

#ifdef USE_PP
#include "preprocessing.h"
static float xIntp[NEW_LEN];
static float zIntp[NEW_LEN];
static float AvgBuffer[NEW_LEN];
#endif

//HMM
#ifdef USE_HMM
#include "Probabilities.h"
#include "ForwardAlgo.h"
static int dirsX[NEW_LEN];
static int dirsZ[NEW_LEN];
#endif

//ML
#ifdef USE_ML
//#include "LinearSVM.h"
#include "RandomForest.h"
static float MLVectors[NEW_LEN*2];
#endif

//DTW
#ifdef USE_DTW
#include "DTW_Algos.h"
#include "DTW_Models.h"
static float AccuCost[NEW_LEN][NEW_LEN];
#endif

/* USER CODE BEGIN Includes */

#define ACCEL_ADDR 0xD0 //0x68

/* USER CODE END Includes */
uint8_t myArr[12] = "Hello World\n";
char cInitMsg[] = "Initialize MPU6050\r\n";
/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART2_UART_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void ClearVec(float * data, int len){
	for(int i = 0; i<len; i++){
		data[i] = 0.0;
	}
}

void DoPP(float * RawDat, int len, float * IntpDat, float* Buffer, float fMin, float fMax){
	OffsetCorr(RawDat,len);
	Interpolate(RawDat, IntpDat, len, NEW_LEN);
	MovingAverage(IntpDat, Buffer, NEW_LEN, MASK_SIZE);
	Normalize(IntpDat, NEW_LEN, NORMALIZE_VAL, fMin, fMax);
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
	uint8_t data = 0x01;
#ifdef SEND_TO_MASTER
	uint8_t retVal[4];
	char cSendStr[100];
	char cStart[] = "Start\r\n";
	char cStop[] = "Stop\r\n";
#endif
	uint8_t cMotionVals[14];
	HAL_StatusTypeDef result;
	uint8_t bNewSet = 0;
	int16_t x,y,z;
	static float xv[MAX_VEC_LEN] = {0};
	//float yv[MAX_VEC_LEN] = {0};
	static float zv[MAX_VEC_LEN] = {0};
	float fMinx, fMaxx, fMinz, fMaxz;
	int idx = 0;
	uint8_t bStartPrediction = 0;
	uint8_t bVectorsNonEmpty = 1;
	int volatile predictionResult = 0;
	int volatile pathLenX = 0, CostX = 0;
	int volatile PathLenZ = 0, CostZ = 0;
	float volatile Prob = 0;
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */
  //HAL_UART_Transmit(&huart2, myArr, 12, 10);
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  data = MPU6050_PWR1_CLKSEL_BIT;
  result = HAL_I2C_Mem_Write(&hi2c1, ACCEL_ADDR, MPU6050_RA_PWR_MGMT_1, I2C_MEMADD_SIZE_8BIT, &data, 1, 1000);
  //retVal[0] = result; HAL_UART_Transmit(&huart2, &data, 1, 10);
  data = MPU6050_GYRO_FS_250;
  result = HAL_I2C_Mem_Write(&hi2c1, ACCEL_ADDR, MPU6050_RA_GYRO_CONFIG, I2C_MEMADD_SIZE_8BIT, &data, 1, 1000);
  //retVal[1] = result; HAL_UART_Transmit(&huart2, &data, 1, 10);
  data = MPU6050_ACCEL_FS_2;
  result = HAL_I2C_Mem_Write(&hi2c1, ACCEL_ADDR, MPU6050_RA_ACCEL_CONFIG, I2C_MEMADD_SIZE_8BIT, &data, 1, 1000);
  //retVal[2] = result; HAL_UART_Transmit(&huart2, &data, 1, 10);
  result = HAL_I2C_Mem_Read(&hi2c1, ACCEL_ADDR, MPU6050_RA_WHO_AM_I, I2C_MEMADD_SIZE_8BIT, &data, 1, 1000);
  //retVal[3] = data;
//  cSendStr[2] = ',';
//  cSendStr[5] = ',';
//  cSendStr[8] = '\n';
HAL_UART_Transmit(&huart2, (uint8_t*)cInitMsg, strlen(cInitMsg), 100);
  while (1)
  {
#ifdef SEND_TO_MASTER
	  result = HAL_I2C_Mem_Read(&hi2c1, ACCEL_ADDR, MPU6050_RA_ACCEL_XOUT_H, I2C_MEMADD_SIZE_8BIT, cMotionVals, 14, 1000);
 
	  if(!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13)){
		if(!bNewSet){
			HAL_UART_Transmit(&huart2, (uint8_t*)cStart, strlen(cStart), 100);
			bNewSet = 1;
		}
		x = (((int16_t)cMotionVals[0]) << 8); x+= cMotionVals[1];
		y = (((int16_t)cMotionVals[2]) << 8); y+= cMotionVals[3];
		z = (((int16_t)cMotionVals[4]) << 8); z+= cMotionVals[5];
		sprintf(cSendStr,"%d,%d,%d\r\n", x, y, z);
		HAL_UART_Transmit(&huart2, (uint8_t*)cSendStr, strlen(cSendStr), 100);
	  //HAL_UART_Transmit(&huart2, myArr, 12, 10);
		  
	  }else if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) && bNewSet){
		  bNewSet = 0;
		  HAL_UART_Transmit(&huart2, (uint8_t*)cStop, sizeof(cStop), 100);
	  }
	  HAL_Delay(20);
#else
	  
	if(bVectorsNonEmpty){
		ClearVec(xv,MAX_VEC_LEN);
		ClearVec(zv,MAX_VEC_LEN);
		bVectorsNonEmpty = 0;
	}
	result = HAL_I2C_Mem_Read(&hi2c1, ACCEL_ADDR, MPU6050_RA_ACCEL_XOUT_H, I2C_MEMADD_SIZE_8BIT, cMotionVals, 14, 1000);
 
	// If User-Button is pressed
	if(!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13)){
		if(!bNewSet){
			bNewSet = 1;
			idx = 0;
			bStartPrediction = 0;
		}
		if(idx < MAX_VEC_LEN){
			x = (((int16_t)cMotionVals[0]) << 8); x+= cMotionVals[1];
			//y = (((int16_t)cMotionVals[2]) << 8); y+= cMotionVals[3];
			z = (((int16_t)cMotionVals[4]) << 8); z+= cMotionVals[5];
			if(idx == 0){
				fMinx = x; fMaxx = 0;
				fMinz = z; fMaxz = 0;
			}else{
				if(x > fMaxx) fMaxx = x;
				if(z > fMaxz) fMaxz = z;
				if(x < fMinx) fMinx = x;
				if(z < fMinz) fMinz = z;
			}
			xv[idx] = (float)x;
			zv[idx] = (float)z;
			idx++;
		}
	}else if(bNewSet){
		bStartPrediction = 1;
		bNewSet = 0;
	}
	
	if(!bStartPrediction){
		HAL_Delay(20);
		continue;
	}
#ifdef USE_PP

	// Preprocessing
	/*OffsetCorr(xv,idx);
	Interpolate(xv, xIntp, idx, NEW_LEN);
	MovingAverage(xIntp, AvgBuffer, NEW_LEN, 5);
	Normalize(xIntp, NEW_LEN, 100, fMinx, fMaxx);
	CalcPitch(xIntp, NEW_LEN, AvgBuffer, 5);
	ClassifyPitch(xIntp, NEW_LEN, AvgBuffer, dirsX);*/
	DoPP(xv, idx, xIntp, AvgBuffer, fMinx, fMaxx);
	DoPP(zv, idx, zIntp, AvgBuffer, fMinz, fMaxz);
#endif
#ifdef USE_ML
	for(int i=0;i<NEW_LEN;i++){
		MLVectors[i*2]=xIntp[i];
		MLVectors[(i*2)+1]=zIntp[i];
	}
	predictionResult = predict(MLVectors);
#endif
#ifdef USE_HMM
	CalcPitch(xIntp, NEW_LEN, AvgBuffer, 5);
	ClassifyPitch(xIntp, NEW_LEN, AvgBuffer, dirsX);
	CalcPitch(zIntp, NEW_LEN, AvgBuffer, 5);
	ClassifyPitch(zIntp, NEW_LEN, AvgBuffer, dirsZ);
	for(int i=0; i<NEW_LEN; i++){
		dirsX[i] = ClassifyDir(dirsX[i], dirsZ[i]);
	}
	Prob = (float)CalcForward(dirsX);
#endif
#ifdef USE_DTW
	/*AccumulatedCostMatrix(AccuCost, dtw3_x, xIntp);
	pathLenX = FindWarpingPathBackwards(AccuCost);
	CostX = AccuCost[49][49];
	AccumulatedCostMatrix(AccuCost, dtw3_z, zIntp);
	PathLenZ = FindWarpingPathBackwards(AccuCost);
	CostZ = AccuCost[49][49];*/
	Prob = (float)CalcPath(AccuCost, xIntp, zIntp);
#endif
	bStartPrediction = 0;
	bVectorsNonEmpty = 1;
	  
#endif	  
	  
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00707CBB;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Analogue filter 
    */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Digital filter 
    */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

