#define LEN 50
#define MAX_PATH_LEN 16000
#include "DTW_Models.h"

//Distance Function
float CalcDistance(float DTWVec, float Input){
	return (DTWVec - Input)*(DTWVec - Input);
}

void CheckMin(float dtw[LEN][LEN], int *idxi, int *idxj){
	int i = *idxi;
	int j = *idxj;
	if(dtw[i-1][j-1] <= dtw[i-1][j] && dtw[i-1][j-1] <= dtw[i][j-1]){
		*idxi -= 1;
		*idxj -= 1;
	}else if(dtw[i-1][j] <= dtw[i-1][j-1] && dtw[i-1][j] <= dtw[i][j-1]){
		*idxi -= 1;
	}else{
		*idxj -= 1;
	}
}

void AccumulatedCostMatrix(float acc[LEN][LEN], float* dtw, float* input){
	acc[0][0] = 0;
	int idxi, idxj;
	for(int i=1; i<LEN; i++){
		acc[i][0] = acc[i-1][0]+CalcDistance(dtw[i], input[0]);
		acc[0][i] = acc[0][i-1]+CalcDistance(dtw[0], input[i]);
	}
	for(int i=1;i<LEN;i++){
		for(int j=1;j<LEN;j++){
			idxi = i;
			idxj = j;
			CheckMin(acc, &idxi, &idxj);
			acc[i][j] = CalcDistance(dtw[i], input[j]) + acc[idxi][idxj];
		}
	}
}

int FindWarpingPathBackwards(float acc[LEN][LEN]){
	int pathLength = 0;
	int i = LEN, j = LEN;
	while(i > 1 || j > 1){
		pathLength++;
		if(i >= 1 && j >= 1){
			CheckMin(acc, &i,&j);
		}else if(i == 0){
			j -= 1;
		}else if(j == 0){
			i -= 1;
		}
	}
	return pathLength;
}

int CalcPath(float acc[LEN][LEN], float* xVec, float* zVec){
	float *DTWx, *DTWz;
	int pathLenX = 0;
	float CostX, CostZ;
	float CostSum=0;
	int idx = 99;
	for(int i=0; i<5; i++){
		switch(i){
			case 0:
				DTWx = dtw1_x;
				DTWz = dtw1_z;
				break;
			case 1:
				DTWx = dtw3_x;
				DTWz = dtw3_z;
				break;
			case 2:
				DTWx = dtwkr_x;
				DTWz = dtwkr_z;
				break;
			case 3:
				DTWx = dtw4_x;
				DTWz = dtw4_z;
				break;
			case 4:
				DTWx = dtw6_x;
				DTWz = dtw6_z;
				break;
		};
		
		AccumulatedCostMatrix(acc, DTWx, xVec);
		//pathLenX = FindWarpingPathBackwards(AccuCost);
		CostX = acc[LEN-1][LEN-1];
		if(CostX > MAX_PATH_LEN) continue;
		AccumulatedCostMatrix(acc, DTWz, zVec);
		CostZ = acc[LEN-1][LEN-1];
		if(CostZ > MAX_PATH_LEN) continue;
		
		if(CostSum < (CostX+CostZ)){
			CostSum = (CostX+CostZ);
			idx = i;
		}
	}
	return idx;
}
