#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int predict_0(float features[]) {
    int classes[5];
    
    if (features[78] <= -40.239999771118164) {
        if (features[49] <= -5.285000801086426) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 2; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[44] <= -22.989999771118164) {
            if (features[27] <= 23.880000114440918) {
                classes[0] = 12; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 4; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[53] <= 59.595001220703125) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 3; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_1(float features[]) {
    int classes[5];
    
    if (features[80] <= -28.639999389648438) {
        if (features[71] <= -3.315000534057617) {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 7; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[60] <= -20.124999046325684) {
            if (features[55] <= 41.82000017166138) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 4; 
            }
        } else {
            classes[0] = 12; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_2(float features[]) {
    int classes[5];
    
    if (features[13] <= 12.179999828338623) {
        if (features[48] <= -12.465000107884407) {
            if (features[67] <= -45.609999656677246) {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[78] <= 27.9950008392334) {
                if (features[62] <= 33.894999504089355) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 5; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            }
        }
    } else {
        if (features[67] <= -11.5) {
            classes[0] = 0; 
            classes[1] = 12; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 1; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_3(float features[]) {
    int classes[5];
    
    if (features[53] <= 3.9749999046325684) {
        if (features[51] <= -53.29500198364258) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 9; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[44] <= 2.5550003051757812) {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 3; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[16] <= 18.110000133514404) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            classes[0] = 0; 
            classes[1] = 12; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_4(float features[]) {
    int classes[5];
    
    if (features[40] <= 35.72999954223633) {
        if (features[10] <= 31.190000534057617) {
            if (features[51] <= -12.704999685287476) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 10; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[55] <= 2.929999828338623) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 3; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 7; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            if (features[24] <= 2.4300003051757812) {
                classes[0] = 0; 
                classes[1] = 1; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 9; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_5(float features[]) {
    int classes[5];
    
    if (features[49] <= -33.040000915527344) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 9; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[48] <= 20.579999208450317) {
            if (features[18] <= 13.34999942779541) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 1; 
                classes[4] = 0; 
            } else {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[54] <= 25.21500015258789) {
                if (features[47] <= 23.210000038146973) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_6(float features[]) {
    int classes[5];
    
    if (features[60] <= 17.489999771118164) {
        if (features[80] <= 38.94000053405762) {
            if (features[61] <= -34.50500011444092) {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    } else {
        if (features[87] <= 13.080000400543213) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 5; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 12; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_7(float features[]) {
    int classes[5];
    
    if (features[58] <= -24.15499973297119) {
        if (features[71] <= -67.54499816894531) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 8; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 4; 
        }
    } else {
        if (features[51] <= -21.045000076293945) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 9; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[31] <= -25.40000057220459) {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 12; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_8(float features[]) {
    int classes[5];
    
    if (features[57] <= 54.16500186920166) {
        if (features[83] <= 64.26999855041504) {
            if (features[43] <= 35.03499889373779) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 9; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[48] <= 11.90999984741211) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 1; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 8; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            if (features[46] <= 1.654998779296875) {
                classes[0] = 4; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_9(float features[]) {
    int classes[5];
    
    if (features[67] <= -55.689998626708984) {
        if (features[56] <= 22.325000762939453) {
            classes[0] = 8; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[86] <= 23.645000457763672) {
            if (features[34] <= -47.10499954223633) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 6; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[38] <= -41.359999656677246) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_10(float features[]) {
    int classes[5];
    
    if (features[41] <= 15.574999809265137) {
        if (features[39] <= -12.224999904632568) {
            if (features[67] <= 22.725000381469727) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 5; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 4; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[75] <= -72.3499984741211) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 1; 
                classes[4] = 0; 
            } else {
                classes[0] = 14; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[45] <= 44.85999870300293) {
            if (features[64] <= 22.294999599456787) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 10; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 5; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_11(float features[]) {
    int classes[5];
    
    if (features[65] <= -60.57000160217285) {
        if (features[82] <= -43.77499961853027) {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 8; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[61] <= 45.94500160217285) {
            if (features[18] <= -8.020000100135803) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 7; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_12(float features[]) {
    int classes[5];
    
    if (features[78] <= -65.29500007629395) {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[88] <= 14.105000019073486) {
            if (features[49] <= -35.75) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[24] <= 20.169999718666077) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_13(float features[]) {
    int classes[5];
    
    if (features[57] <= 60.21500110626221) {
        if (features[58] <= -45.75) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 8; 
            classes[4] = 0; 
        } else {
            if (features[10] <= 28.850000381469727) {
                if (features[72] <= -56.709999084472656) {
                    classes[0] = 0; 
                    classes[1] = 4; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 10; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_14(float features[]) {
    int classes[5];
    
    if (features[50] <= 6.6499998569488525) {
        if (features[16] <= 31.405000686645508) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 2; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 13; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[80] <= -48.994998931884766) {
            classes[0] = 0; 
            classes[1] = 12; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[49] <= 29.66999912261963) {
                if (features[21] <= 12.180000305175781) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_15(float features[]) {
    int classes[5];
    
    if (features[44] <= 68.4000015258789) {
        if (features[47] <= -28.005000114440918) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 10; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[74] <= -28.19499969482422) {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[56] <= -23.844999313354492) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 8; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_16(float features[]) {
    int classes[5];
    
    if (features[45] <= 45.35500144958496) {
        if (features[40] <= 41.72499942779541) {
            if (features[14] <= 42.579999923706055) {
                if (features[25] <= -0.38500022888183594) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 11; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[80] <= -50.70500040054321) {
                    classes[0] = 0; 
                    classes[1] = 1; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 6; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 7; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 11; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_17(float features[]) {
    int classes[5];
    
    if (features[29] <= -70.28500366210938) {
        if (features[74] <= -20.299999713897705) {
            classes[0] = 0; 
            classes[1] = 11; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 2; 
        }
    } else {
        if (features[25] <= 17.8100004196167) {
            if (features[18] <= 5.625) {
                if (features[68] <= -55.21999931335449) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 2; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 13; 
                    classes[4] = 0; 
                }
            } else {
                if (features[68] <= -17.100000381469727) {
                    if (features[66] <= -23.84000015258789) {
                        classes[0] = 0; 
                        classes[1] = 1; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 1; 
                    }
                } else {
                    classes[0] = 4; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 6; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_18(float features[]) {
    int classes[5];
    
    if (features[53] <= -11.765000104904175) {
        if (features[18] <= 75.875) {
            if (features[71] <= -44.04499936103821) {
                classes[0] = 1; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 7; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[63] <= -27.005000114440918) {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[51] <= 44.91999912261963) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 12; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 3; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_19(float features[]) {
    int classes[5];
    
    if (features[85] <= 26.09000015258789) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 13; 
    } else {
        if (features[35] <= 26.619999885559082) {
            if (features[51] <= 4.6949999034404755) {
                classes[0] = 5; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 6; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[86] <= -37.52999973297119) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 6; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 10; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_20(float features[]) {
    int classes[5];
    
    if (features[80] <= -53.18499946594238) {
        if (features[73] <= 15.855000495910645) {
            classes[0] = 0; 
            classes[1] = 17; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 1; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[65] <= -63.23500061035156) {
            classes[0] = 6; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[38] <= -6.37000036239624) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[53] <= 43.015000343322754) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_21(float features[]) {
    int classes[5];
    
    if (features[86] <= 24.760000228881836) {
        if (features[45] <= 23.700000762939453) {
            if (features[10] <= 24.780000686645508) {
                if (features[73] <= -33.45000123977661) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 7; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 8; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_22(float features[]) {
    int classes[5];
    
    if (features[16] <= 51.64999961853027) {
        if (features[51] <= -10.39499968290329) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 9; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[82] <= 25.440000534057617) {
                if (features[63] <= -36.42500114440918) {
                    classes[0] = 0; 
                    classes[1] = 4; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 5; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        }
    } else {
        if (features[76] <= -50.55999827384949) {
            classes[0] = 0; 
            classes[1] = 3; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 13; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_23(float features[]) {
    int classes[5];
    
    if (features[58] <= -20.050000190734863) {
        if (features[85] <= 37.88999938964844) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 10; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 6; 
            classes[4] = 0; 
        }
    } else {
        if (features[23] <= -15.039999961853027) {
            classes[0] = 0; 
            classes[1] = 7; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[77] <= -1.9449996948242188) {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[86] <= 6.34499979019165) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 7; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 1; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_24(float features[]) {
    int classes[5];
    
    if (features[58] <= -21.794999599456787) {
        if (features[49] <= 28.529998779296875) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 10; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        }
    } else {
        if (features[27] <= 16.799999713897705) {
            if (features[22] <= 40.40999984741211) {
                classes[0] = 0; 
                classes[1] = 5; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 9; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_25(float features[]) {
    int classes[5];
    
    if (features[76] <= -38.26499938964844) {
        if (features[61] <= 1.635000228881836) {
            classes[0] = 0; 
            classes[1] = 11; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 1; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[14] <= 20.65999984741211) {
            if (features[95] <= -2.4149998873472214) {
                if (features[67] <= -0.18999958038330078) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 9; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[48] <= 9.920000076293945) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 4; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            }
        } else {
            classes[0] = 9; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_26(float features[]) {
    int classes[5];
    
    if (features[80] <= 38.94000053405762) {
        if (features[24] <= -37.95000076293945) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 8; 
            classes[4] = 0; 
        } else {
            if (features[25] <= 16.44000005722046) {
                if (features[32] <= -47.0) {
                    classes[0] = 0; 
                    classes[1] = 6; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 10; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 9; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_27(float features[]) {
    int classes[5];
    
    if (features[84] <= 34.55000019073486) {
        if (features[16] <= 17.764999866485596) {
            if (features[85] <= 38.83999824523926) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            }
        } else {
            if (features[56] <= 21.010000705718994) {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[73] <= -0.6099996566772461) {
                    classes[0] = 0; 
                    classes[1] = 4; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_28(float features[]) {
    int classes[5];
    
    if (features[78] <= 20.47499942779541) {
        if (features[24] <= 59.84000015258789) {
            if (features[36] <= -43.28499984741211) {
                if (features[65] <= -59.18000030517578) {
                    if (features[76] <= -32.714999198913574) {
                        classes[0] = 0; 
                        classes[1] = 7; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 8; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_29(float features[]) {
    int classes[5];
    
    if (features[66] <= 39.51999855041504) {
        if (features[78] <= 23.809999465942383) {
            if (features[18] <= 16.235000133514404) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            } else {
                if (features[30] <= -13.889999747276306) {
                    classes[0] = 0; 
                    classes[1] = 8; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 4; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_30(float features[]) {
    int classes[5];
    
    if (features[23] <= 6.404999852180481) {
        if (features[59] <= 38.5) {
            if (features[90] <= 16.414999961853027) {
                if (features[12] <= 7.174999952316284) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    if (features[23] <= -42.69500017166138) {
                        if (features[44] <= 15.28499984741211) {
                            classes[0] = 0; 
                            classes[1] = 1; 
                            classes[2] = 0; 
                            classes[3] = 0; 
                            classes[4] = 0; 
                        } else {
                            classes[0] = 0; 
                            classes[1] = 0; 
                            classes[2] = 0; 
                            classes[3] = 1; 
                            classes[4] = 0; 
                        }
                    } else {
                        classes[0] = 4; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                }
            } else {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_31(float features[]) {
    int classes[5];
    
    if (features[13] <= 15.970000267028809) {
        if (features[40] <= 53.130001068115234) {
            if (features[24] <= 59.84000015258789) {
                if (features[23] <= 3.8450002670288086) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 5; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 7; 
        }
    } else {
        if (features[47] <= 0.20999908447265625) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 2; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 11; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_32(float features[]) {
    int classes[5];
    
    if (features[13] <= 12.394999980926514) {
        if (features[35] <= 21.920000076293945) {
            if (features[83] <= 36.54499912261963) {
                if (features[82] <= 8.02500057220459) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 8; 
                }
            } else {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[5] <= 2.880000114440918) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 10; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[20] <= 1.880000114440918) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 1; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_33(float features[]) {
    int classes[5];
    
    if (features[31] <= 29.700000762939453) {
        if (features[24] <= 32.97499942779541) {
            if (features[22] <= -9.150000095367432) {
                if (features[78] <= 27.9950008392334) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 9; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 9; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[62] <= -8.305000305175781) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 1; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 10; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_34(float features[]) {
    int classes[5];
    
    if (features[40] <= 41.72499942779541) {
        if (features[72] <= -12.029999732971191) {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[24] <= 28.225000381469727) {
                if (features[68] <= 29.625000953674316) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 4; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[74] <= 33.250000953674316) {
                    classes[0] = 10; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 9; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_35(float features[]) {
    int classes[5];
    
    if (features[71] <= 4.440000534057617) {
        if (features[33] <= 14.544999599456787) {
            if (features[22] <= 40.40999984741211) {
                if (features[44] <= 56.239999771118164) {
                    if (features[66] <= -44.38999938964844) {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 1; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 5; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 6; 
                }
            } else {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 7; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 11; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_36(float features[]) {
    int classes[5];
    
    if (features[40] <= 47.40999984741211) {
        if (features[25] <= 19.72000026702881) {
            if (features[60] <= -21.489999294281006) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 11; 
                classes[4] = 0; 
            } else {
                if (features[78] <= -33.94999980926514) {
                    classes[0] = 0; 
                    classes[1] = 6; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 6; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 7; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_37(float features[]) {
    int classes[5];
    
    if (features[84] <= 29.739999294281006) {
        if (features[45] <= -9.809999942779541) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 11; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[56] <= 24.825000762939453) {
                if (features[62] <= -20.439999103546143) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 5; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_38(float features[]) {
    int classes[5];
    
    if (features[84] <= 27.385000228881836) {
        if (features[22] <= -39.2549991607666) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 8; 
            classes[4] = 0; 
        } else {
            if (features[75] <= -2.0500001907348633) {
                if (features[58] <= 25.96500015258789) {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 8; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[63] <= -77.53499984741211) {
                    classes[0] = 1; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 8; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_39(float features[]) {
    int classes[5];
    
    if (features[57] <= 60.21500110626221) {
        if (features[72] <= -3.5899996757507324) {
            if (features[54] <= 28.764999389648438) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 1; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 15; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[4] <= 1.5299999713897705) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[86] <= 2.2799999117851257) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 2; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_40(float features[]) {
    int classes[5];
    
    if (features[22] <= -23.869999885559082) {
        if (features[82] <= 25.440000534057617) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 13; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 1; 
        }
    } else {
        if (features[53] <= 7.934999704360962) {
            if (features[86] <= -39.78499984741211) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 6; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[79] <= -9.039999842643738) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 4; 
            } else {
                classes[0] = 0; 
                classes[1] = 6; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_41(float features[]) {
    int classes[5];
    
    if (features[47] <= -22.019999980926514) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 9; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[49] <= 3.509999990463257) {
            if (features[48] <= 38.559998750686646) {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 1; 
                classes[4] = 0; 
            }
        } else {
            if (features[36] <= -46.880001068115234) {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[63] <= 14.890000343322754) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 5; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 7; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_42(float features[]) {
    int classes[5];
    
    if (features[42] <= 67.43500137329102) {
        if (features[51] <= -53.29500198364258) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 7; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[55] <= -19.414999961853027) {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[18] <= 11.474999904632568) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    if (features[87] <= 81.3650016784668) {
                        classes[0] = 0; 
                        classes[1] = 4; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 13; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_43(float features[]) {
    int classes[5];
    
    if (features[40] <= 53.130001068115234) {
        if (features[18] <= -2.0999999046325684) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 7; 
            classes[4] = 0; 
        } else {
            if (features[47] <= -28.005000114440918) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[76] <= -41.84999918937683) {
                    classes[0] = 0; 
                    classes[1] = 5; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 8; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 12; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_44(float features[]) {
    int classes[5];
    
    if (features[27] <= 20.05999994277954) {
        if (features[83] <= 31.3100004196167) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        } else {
            if (features[40] <= 10.350000143051147) {
                if (features[24] <= 26.005000114440918) {
                    classes[0] = 0; 
                    classes[1] = 5; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[77] <= -11.604999542236328) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 1; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 14; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_45(float features[]) {
    int classes[5];
    
    if (features[25] <= 20.524999618530273) {
        if (features[22] <= 39.91499996185303) {
            if (features[49] <= 29.66999912261963) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            } else {
                if (features[16] <= 23.244999408721924) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 7; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 7; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 8; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_46(float features[]) {
    int classes[5];
    
    if (features[25] <= 17.8100004196167) {
        if (features[44] <= -22.18000030517578) {
            if (features[52] <= 40.329999923706055) {
                classes[0] = 11; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 1; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[47] <= 14.864999771118164) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            } else {
                if (features[56] <= 14.065000534057617) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 5; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_47(float features[]) {
    int classes[5];
    
    if (features[27] <= 22.505000114440918) {
        if (features[65] <= -39.96000099182129) {
            if (features[72] <= -3.5899996757507324) {
                classes[0] = 0; 
                classes[1] = 11; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 4; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[83] <= 40.24499988555908) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[85] <= 79.42000007629395) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 11; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 1; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_48(float features[]) {
    int classes[5];
    
    if (features[59] <= 37.450000286102295) {
        if (features[58] <= -36.72999954223633) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 11; 
            classes[4] = 0; 
        } else {
            if (features[55] <= -53.42499923706055) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[25] <= -31.975000858306885) {
                    classes[0] = 0; 
                    classes[1] = 2; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 15; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_49(float features[]) {
    int classes[5];
    
    if (features[84] <= 34.55000019073486) {
        if (features[18] <= 7.03000020980835) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 11; 
            classes[4] = 0; 
        } else {
            if (features[16] <= 59.795000076293945) {
                if (features[67] <= -51.17000102996826) {
                    classes[0] = 0; 
                    classes[1] = 6; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 8; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 6; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 9; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_50(float features[]) {
    int classes[5];
    
    if (features[72] <= -55.21999931335449) {
        classes[0] = 0; 
        classes[1] = 11; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[57] <= 54.16500186920166) {
            if (features[20] <= 52.0049991607666) {
                if (features[75] <= -7.414999961853027) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 5; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 10; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_51(float features[]) {
    int classes[5];
    
    if (features[11] <= 16.980000019073486) {
        if (features[65] <= -67.34000015258789) {
            classes[0] = 6; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[58] <= -6.255000114440918) {
                if (features[47] <= 30.90499973297119) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            } else {
                if (features[53] <= -29.11500072479248) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 1; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 15; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_52(float features[]) {
    int classes[5];
    
    if (features[60] <= -20.124999046325684) {
        if (features[88] <= 12.135000228881836) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 10; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 5; 
        }
    } else {
        if (features[46] <= 2.3850001096725464) {
            if (features[65] <= -63.23500061035156) {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 8; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_53(float features[]) {
    int classes[5];
    
    if (features[13] <= 13.675000190734863) {
        if (features[12] <= 32.02500057220459) {
            if (features[49] <= -24.73999971151352) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[78] <= 19.479999542236328) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 7; 
                }
            }
        } else {
            classes[0] = 9; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_54(float features[]) {
    int classes[5];
    
    if (features[80] <= 30.15999984741211) {
        if (features[20] <= -10.349999904632568) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 8; 
            classes[4] = 0; 
        } else {
            if (features[56] <= 22.325000762939453) {
                if (features[57] <= -90.79499816894531) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 10; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[58] <= 53.61000061035156) {
                    classes[0] = 0; 
                    classes[1] = 5; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_55(float features[]) {
    int classes[5];
    
    if (features[63] <= -27.7450008392334) {
        if (features[76] <= -23.644999265670776) {
            classes[0] = 0; 
            classes[1] = 13; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 8; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[71] <= 18.599998950958252) {
            if (features[53] <= 64.06000137329102) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 4; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 6; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_56(float features[]) {
    int classes[5];
    
    if (features[47] <= -27.744999885559082) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 12; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[58] <= 21.6850004196167) {
            if (features[86] <= 30.494999885559082) {
                if (features[12] <= 30.065000534057617) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 8; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 4; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 5; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 11; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_57(float features[]) {
    int classes[5];
    
    if (features[43] <= 31.999999046325684) {
        if (features[47] <= -27.744999885559082) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 11; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[49] <= 36.625) {
                if (features[24] <= 13.200000405311584) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 6; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            }
        }
    } else {
        if (features[65] <= -29.075000762939453) {
            classes[0] = 0; 
            classes[1] = 11; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 1; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_58(float features[]) {
    int classes[5];
    
    if (features[84] <= 34.55000019073486) {
        if (features[65] <= -63.43000030517578) {
            if (features[50] <= 27.885000944137573) {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 6; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[29] <= -21.75499999523163) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 5; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 10; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_59(float features[]) {
    int classes[5];
    
    if (features[20] <= -29.579999923706055) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 9; 
        classes[4] = 0; 
    } else {
        if (features[27] <= 20.05999994277954) {
            if (features[24] <= 24.05500030517578) {
                if (features[38] <= -7.289999008178711) {
                    classes[0] = 0; 
                    classes[1] = 7; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    if (features[66] <= -14.289999961853027) {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 7; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 1; 
                        classes[4] = 0; 
                    }
                }
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 8; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_60(float features[]) {
    int classes[5];
    
    if (features[47] <= -19.18499994277954) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 9; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[76] <= -34.94999885559082) {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[37] <= 41.209999084472656) {
                if (features[58] <= -24.15499973297119) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 6; 
                } else {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_61(float features[]) {
    int classes[5];
    
    if (features[50] <= 2.805000066757202) {
        if (features[22] <= 31.799999713897705) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 1; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 11; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[80] <= 25.25499963760376) {
            if (features[12] <= 6.740000009536743) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            } else {
                if (features[69] <= -18.76999855041504) {
                    classes[0] = 0; 
                    classes[1] = 6; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 8; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_62(float features[]) {
    int classes[5];
    
    if (features[22] <= 48.66499900817871) {
        if (features[61] <= 29.940001010894775) {
            if (features[47] <= 25.734999656677246) {
                if (features[64] <= 14.434999704360962) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 4; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 9; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 10; 
        }
    } else {
        classes[0] = 10; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_63(float features[]) {
    int classes[5];
    
    if (features[80] <= -80.875) {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[35] <= -15.895000457763672) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            if (features[63] <= -32.135000228881836) {
                if (features[61] <= -85.47999954223633) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 11; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[41] <= -18.195000410079956) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 5; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 5; 
                    classes[4] = 0; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_64(float features[]) {
    int classes[5];
    
    if (features[72] <= -43.28499984741211) {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[42] <= -12.890000343322754) {
            if (features[47] <= -27.744999885559082) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 5; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[39] <= -7.350000083446503) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            } else {
                if (features[80] <= -42.05999946594238) {
                    classes[0] = 0; 
                    classes[1] = 1; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 9; 
                    classes[4] = 0; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_65(float features[]) {
    int classes[5];
    
    if (features[73] <= 0.49499988555908203) {
        if (features[76] <= -34.94999885559082) {
            classes[0] = 0; 
            classes[1] = 7; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[44] <= -31.78499984741211) {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[61] <= 19.704999446868896) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 8; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            }
        }
    } else {
        if (features[49] <= -23.15500056743622) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 12; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 1; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_66(float features[]) {
    int classes[5];
    
    if (features[53] <= 75.30500030517578) {
        if (features[45] <= 23.700000762939453) {
            if (features[56] <= -32.679999351501465) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                if (features[69] <= -28.479999542236328) {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 9; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 8; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 9; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_67(float features[]) {
    int classes[5];
    
    if (features[58] <= -55.41499900817871) {
        if (features[72] <= -27.71500015258789) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 2; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 16; 
            classes[4] = 0; 
        }
    } else {
        if (features[22] <= 46.0099983215332) {
            if (features[65] <= -34.09000015258789) {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[78] <= -4.680000305175781) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 2; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            }
        } else {
            classes[0] = 8; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_68(float features[]) {
    int classes[5];
    
    if (features[51] <= -17.389999866485596) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 9; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[61] <= 19.704999446868896) {
            if (features[76] <= -34.94999885559082) {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[14] <= 11.235000014305115) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 9; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_69(float features[]) {
    int classes[5];
    
    if (features[58] <= 16.8100004196167) {
        if (features[35] <= -8.785000443458557) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            if (features[5] <= 1.740000069141388) {
                if (features[70] <= 39.25) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 3; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[58] <= -36.72999954223633) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 1; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 6; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        if (features[27] <= -22.89000129699707) {
            classes[0] = 0; 
            classes[1] = 15; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 3; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_70(float features[]) {
    int classes[5];
    
    if (features[76] <= -38.53999900817871) {
        classes[0] = 0; 
        classes[1] = 12; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[88] <= 26.625) {
            if (features[65] <= -63.23500061035156) {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[38] <= -18.82499933242798) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 7; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_71(float features[]) {
    int classes[5];
    
    if (features[76] <= -56.744998931884766) {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[10] <= 27.6850004196167) {
            if (features[22] <= 2.0250000953674316) {
                if (features[85] <= 37.88999938964844) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 9; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 6; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 10; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_72(float features[]) {
    int classes[5];
    
    if (features[34] <= -47.10499954223633) {
        if (features[73] <= 9.09000015258789) {
            if (features[68] <= -11.329999923706055) {
                classes[0] = 0; 
                classes[1] = 10; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 2; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 8; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[22] <= -32.614999771118164) {
            if (features[38] <= 55.67499923706055) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 12; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 1; 
            }
        } else {
            if (features[28] <= 35.47999954223633) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 3; 
            } else {
                classes[0] = 4; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_73(float features[]) {
    int classes[5];
    
    if (features[71] <= -7.644999742507935) {
        if (features[84] <= 39.34999942779541) {
            if (features[22] <= 40.40999984741211) {
                if (features[93] <= 5.584999918937683) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 5; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 6; 
        }
    } else {
        if (features[81] <= 73.67000198364258) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 14; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 1; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_74(float features[]) {
    int classes[5];
    
    if (features[85] <= 38.83999824523926) {
        if (features[62] <= 0.12999916076660156) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 8; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[67] <= -49.795000076293945) {
            if (features[34] <= -54.71500015258789) {
                classes[0] = 0; 
                classes[1] = 9; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 4; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 11; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_75(float features[]) {
    int classes[5];
    
    if (features[51] <= -53.29500198364258) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 11; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[10] <= 27.6850004196167) {
            if (features[37] <= -5.934999465942383) {
                if (features[61] <= 51.560001373291016) {
                    if (features[80] <= -71.80500030517578) {
                        classes[0] = 0; 
                        classes[1] = 1; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 1; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 9; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 5; 
                classes[4] = 0; 
            }
        } else {
            if (features[76] <= -52.299999952316284) {
                classes[0] = 0; 
                classes[1] = 1; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 12; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_76(float features[]) {
    int classes[5];
    
    if (features[58] <= -35.68499946594238) {
        if (features[44] <= 73.04999732971191) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 9; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 7; 
        }
    } else {
        if (features[29] <= -36.49999952316284) {
            classes[0] = 0; 
            classes[1] = 7; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[64] <= 23.604999542236328) {
                if (features[57] <= 6.925000190734863) {
                    classes[0] = 9; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_77(float features[]) {
    int classes[5];
    
    if (features[49] <= -27.565000534057617) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 9; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[40] <= 47.40999984741211) {
            if (features[25] <= -27.424998998641968) {
                if (features[82] <= -27.009999752044678) {
                    classes[0] = 0; 
                    classes[1] = 7; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_78(float features[]) {
    int classes[5];
    
    if (features[33] <= 20.334999561309814) {
        if (features[13] <= 12.394999980926514) {
            if (features[65] <= -34.74499982595444) {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 4; 
            }
        } else {
            if (features[87] <= 13.080000400543213) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 10; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[24] <= 11.475000023841858) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 5; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 12; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_79(float features[]) {
    int classes[5];
    
    if (features[80] <= 30.15999984741211) {
        if (features[27] <= -32.14500093460083) {
            if (features[62] <= 13.159999787807465) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 6; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[71] <= 4.440000534057617) {
                classes[0] = 6; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[63] <= -76.37000274658203) {
                    classes[0] = 1; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 14; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_80(float features[]) {
    int classes[5];
    
    if (features[64] <= 30.464999198913574) {
        if (features[80] <= -51.38999938964844) {
            classes[0] = 0; 
            classes[1] = 11; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[44] <= -31.975000381469727) {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[59] <= 23.489999294281006) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 9; 
                    classes[4] = 0; 
                } else {
                    if (features[46] <= 60.30000114440918) {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 1; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 4; 
                    }
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 8; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_81(float features[]) {
    int classes[5];
    
    if (features[54] <= 25.554999351501465) {
        if (features[35] <= 31.829999923706055) {
            if (features[61] <= 1.2149991989135742) {
                classes[0] = 14; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 4; 
            }
        } else {
            if (features[15] <= -2.650000035762787) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 5; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[10] <= -0.9349999576807022) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 1; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_82(float features[]) {
    int classes[5];
    
    if (features[27] <= 23.880000114440918) {
        if (features[81] <= 10.34000015258789) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            if (features[37] <= 51.44499969482422) {
                if (features[52] <= 16.619999170303345) {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 7; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_83(float features[]) {
    int classes[5];
    
    if (features[37] <= 54.71500015258789) {
        if (features[65] <= -32.255000829696655) {
            if (features[57] <= -9.924999624490738) {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 10; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[42] <= 54.910000801086426) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 9; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 5; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 9; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_84(float features[]) {
    int classes[5];
    
    if (features[45] <= -9.809999942779541) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 13; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[65] <= -40.15500068664551) {
            if (features[50] <= 9.65000057220459) {
                classes[0] = 4; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 11; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[80] <= 20.024999618530273) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_85(float features[]) {
    int classes[5];
    
    if (features[42] <= -38.85499954223633) {
        if (features[55] <= -50.55500030517578) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 15; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[83] <= 39.65499973297119) {
                classes[0] = 0; 
                classes[1] = 1; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 6; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[16] <= 25.265000820159912) {
            if (features[80] <= 23.739999532699585) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 6; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_86(float features[]) {
    int classes[5];
    
    if (features[59] <= 48.8799991607666) {
        if (features[62] <= 2.73499995470047) {
            if (features[37] <= 8.275000095367432) {
                classes[0] = 2; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 14; 
                classes[4] = 0; 
            }
        } else {
            if (features[27] <= 20.05999994277954) {
                if (features[12] <= 44.19499969482422) {
                    classes[0] = 0; 
                    classes[1] = 2; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 4; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_87(float features[]) {
    int classes[5];
    
    if (features[49] <= -42.5) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[43] <= 34.42500019073486) {
            if (features[65] <= -43.86000061035156) {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[37] <= 0.30500030517578125) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 4; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 9; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_88(float features[]) {
    int classes[5];
    
    if (features[50] <= 2.805000066757202) {
        classes[0] = 13; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[16] <= 18.110000133514404) {
            if (features[55] <= 59.17500019073486) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            }
        } else {
            if (features[49] <= -38.454999923706055) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 2; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 9; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_89(float features[]) {
    int classes[5];
    
    if (features[80] <= 23.739999532699585) {
        if (features[22] <= -38.21500015258789) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 8; 
            classes[4] = 0; 
        } else {
            if (features[25] <= 16.44000005722046) {
                if (features[21] <= -7.079999923706055) {
                    if (features[79] <= 27.15999984741211) {
                        classes[0] = 0; 
                        classes[1] = 4; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 1; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 10; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_90(float features[]) {
    int classes[5];
    
    if (features[80] <= -57.39999961853027) {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[88] <= 27.464999198913574) {
            if (features[27] <= -25.49000072479248) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                if (features[73] <= 28.889999389648438) {
                    classes[0] = 8; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_91(float features[]) {
    int classes[5];
    
    if (features[51] <= 5.4999998807907104) {
        if (features[27] <= 21.43499994277954) {
            if (features[50] <= 15.624999761581421) {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 3; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 9; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[18] <= 18.34999990463257) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 6; 
        } else {
            classes[0] = 0; 
            classes[1] = 13; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_92(float features[]) {
    int classes[5];
    
    if (features[84] <= 35.47999954223633) {
        if (features[53] <= 5.214999865740538) {
            if (features[18] <= 48.579999923706055) {
                if (features[23] <= -14.670000553131104) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 4; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[29] <= -49.63999938964844) {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 1; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 12; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_93(float features[]) {
    int classes[5];
    
    if (features[35] <= 19.494999885559082) {
        if (features[46] <= -27.79500102996826) {
            if (features[85] <= 29.244999885559082) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[72] <= -49.51499938964844) {
                classes[0] = 0; 
                classes[1] = 6; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[6] <= -5.56000018119812) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 3; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            }
        }
    } else {
        if (features[75] <= 0.11999988555908203) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 13; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 3; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_94(float features[]) {
    int classes[5];
    
    if (features[10] <= 31.190000534057617) {
        if (features[51] <= -37.65500044822693) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 6; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[56] <= 7.315000534057617) {
                if (features[53] <= 59.595001220703125) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 16; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 3; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 4; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 11; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_95(float features[]) {
    int classes[5];
    
    if (features[59] <= 59.11499786376953) {
        if (features[77] <= 7.599999845027924) {
            if (features[49] <= 26.28499984741211) {
                if (features[40] <= -34.39500045776367) {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 3; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 9; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[79] <= 71.69000053405762) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 11; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 1; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_96(float features[]) {
    int classes[5];
    
    if (features[35] <= -27.06999969482422) {
        if (features[44] <= 42.06000065803528) {
            classes[0] = 0; 
            classes[1] = 2; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 14; 
        }
    } else {
        if (features[10] <= 24.780000686645508) {
            if (features[38] <= -41.359999656677246) {
                if (features[42] <= -52.91000175476074) {
                    if (features[60] <= -1.5149999856948853) {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 6; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 0; 
                    classes[1] = 2; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[47] <= 38.55000019073486) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 1; 
                }
            }
        } else {
            classes[0] = 8; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_97(float features[]) {
    int classes[5];
    
    if (features[55] <= 74.21000099182129) {
        if (features[80] <= -53.18499946594238) {
            classes[0] = 0; 
            classes[1] = 11; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[75] <= -66.2599983215332) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            } else {
                if (features[47] <= -27.744999885559082) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 8; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 9; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_98(float features[]) {
    int classes[5];
    
    if (features[80] <= -53.18499946594238) {
        classes[0] = 0; 
        classes[1] = 12; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[40] <= 41.72499942779541) {
            if (features[10] <= 25.065000534057617) {
                if (features[56] <= 12.194999694824219) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 5; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 3; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 13; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 7; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_99(float features[]) {
    int classes[5];
    
    if (features[16] <= 35.334999084472656) {
        if (features[68] <= -19.585000038146973) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            if (features[56] <= -32.679999351501465) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[56] <= 32.02499961853027) {
            classes[0] = 12; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 7; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_100(float features[]) {
    int classes[5];
    
    if (features[43] <= 35.22000026702881) {
        if (features[64] <= -53.53999900817871) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 9; 
        } else {
            if (features[23] <= -26.71500062942505) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                if (features[55] <= -53.42499923706055) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 7; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_101(float features[]) {
    int classes[5];
    
    if (features[20] <= 51.704999923706055) {
        if (features[42] <= 67.54000091552734) {
            if (features[38] <= -21.849999487400055) {
                if (features[71] <= -23.52500081062317) {
                    classes[0] = 0; 
                    classes[1] = 7; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 2; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        }
    } else {
        if (features[76] <= -37.265000343322754) {
            if (features[29] <= -4.069999694824219) {
                classes[0] = 0; 
                classes[1] = 1; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 12; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_102(float features[]) {
    int classes[5];
    
    if (features[20] <= -9.744999885559082) {
        if (features[92] <= 28.964999198913574) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 14; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 1; 
        }
    } else {
        if (features[78] <= -77.33500099182129) {
            classes[0] = 0; 
            classes[1] = 7; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[65] <= -62.510000228881836) {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[73] <= -1.0299997329711914) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 3; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_103(float features[]) {
    int classes[5];
    
    if (features[67] <= -58.05500030517578) {
        if (features[80] <= -35.709999084472656) {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 7; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[23] <= 3.684999942779541) {
            if (features[51] <= 47.84999942779541) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 8; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 8; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_104(float features[]) {
    int classes[5];
    
    if (features[18] <= 7.195000171661377) {
        if (features[84] <= 23.924999475479126) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 9; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 10; 
        }
    } else {
        if (features[45] <= 22.92500066757202) {
            if (features[64] <= 33.454999923706055) {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 7; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_105(float features[]) {
    int classes[5];
    
    if (features[22] <= 77.32999801635742) {
        if (features[65] <= -60.57000160217285) {
            if (features[77] <= -22.97499942779541) {
                classes[0] = 1; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 10; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[25] <= 6.630000114440918) {
                if (features[78] <= 17.354999542236328) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 8; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 6; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 9; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_106(float features[]) {
    int classes[5];
    
    if (features[40] <= 53.130001068115234) {
        if (features[47] <= -27.744999885559082) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 9; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[27] <= -31.86000084877014) {
                if (features[34] <= -17.19500160217285) {
                    classes[0] = 0; 
                    classes[1] = 5; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 9; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_107(float features[]) {
    int classes[5];
    
    if (features[60] <= -20.929999351501465) {
        if (features[82] <= 21.68000039458275) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 9; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 6; 
        }
    } else {
        if (features[58] <= 21.6850004196167) {
            if (features[85] <= 35.974998474121094) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 13; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[80] <= -51.38999938964844) {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 4; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_108(float features[]) {
    int classes[5];
    
    if (features[55] <= 57.1850004196167) {
        if (features[13] <= 12.394999980926514) {
            if (features[58] <= -45.53499984741211) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            } else {
                if (features[25] <= 16.44000005722046) {
                    classes[0] = 8; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    if (features[31] <= 24.355000495910645) {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 5; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_109(float features[]) {
    int classes[5];
    
    if (features[87] <= 24.260000228881836) {
        if (features[46] <= 60.30000114440918) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 11; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[88] <= 11.265000104904175) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 2; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        }
    } else {
        if (features[49] <= 30.489999771118164) {
            if (features[18] <= 20.559998989105225) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 5; 
                classes[4] = 0; 
            } else {
                classes[0] = 6; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_110(float features[]) {
    int classes[5];
    
    if (features[53] <= -59.14000129699707) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 12; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[60] <= -21.489999294281006) {
            if (features[47] <= 21.894999504089355) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        } else {
            if (features[80] <= -36.67999887466431) {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_111(float features[]) {
    int classes[5];
    
    if (features[51] <= -4.650000095367432) {
        if (features[67] <= -45.609999656677246) {
            classes[0] = 9; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 6; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[76] <= -53.154998779296875) {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[35] <= 1.1949996948242188) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_112(float features[]) {
    int classes[5];
    
    if (features[66] <= 34.97500038146973) {
        if (features[47] <= 4.400000095367432) {
            if (features[64] <= -10.920000076293945) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 2; 
                classes[4] = 0; 
            } else {
                classes[0] = 15; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[20] <= 5.070000052452087) {
                if (features[81] <= 41.014999866485596) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 6; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 2; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 8; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_113(float features[]) {
    int classes[5];
    
    if (features[47] <= -27.744999885559082) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 8; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[80] <= -42.05999946594238) {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[38] <= -32.6850004196167) {
                classes[0] = 9; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[90] <= 21.295000076293945) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 6; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 8; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_114(float features[]) {
    int classes[5];
    
    if (features[16] <= 22.500000476837158) {
        if (features[81] <= 8.585000038146973) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            if (features[57] <= -60.56999969482422) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[78] <= -24.804999351501465) {
            if (features[39] <= -24.789999961853027) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 6; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 9; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_115(float features[]) {
    int classes[5];
    
    if (features[25] <= -18.230000495910645) {
        if (features[56] <= 13.545000553131104) {
            if (features[57] <= 49.79500198364258) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[47] <= -54.74000120162964) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 7; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 9; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_116(float features[]) {
    int classes[5];
    
    if (features[80] <= -83.79000091552734) {
        classes[0] = 0; 
        classes[1] = 8; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[42] <= -12.890000343322754) {
            if (features[66] <= 34.18499946594238) {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[40] <= 47.40999984741211) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_117(float features[]) {
    int classes[5];
    
    if (features[78] <= -67.73999977111816) {
        classes[0] = 0; 
        classes[1] = 11; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[22] <= 3.505000114440918) {
            if (features[80] <= 20.024999618530273) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 5; 
            }
        } else {
            if (features[51] <= -53.29500198364258) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[28] <= -19.820000171661377) {
                    classes[0] = 0; 
                    classes[1] = 2; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_118(float features[]) {
    int classes[5];
    
    if (features[46] <= -28.985000610351562) {
        if (features[72] <= 17.540000438690186) {
            classes[0] = 11; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 4; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[24] <= -1.5999999791383743) {
            if (features[88] <= 20.274999141693115) {
                if (features[80] <= -36.07500076293945) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 8; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_119(float features[]) {
    int classes[5];
    
    if (features[14] <= 51.92499923706055) {
        if (features[23] <= 6.5650001764297485) {
            if (features[80] <= 25.25499963760376) {
                if (features[16] <= 17.764999866485596) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 11; 
                    classes[4] = 0; 
                } else {
                    if (features[58] <= 7.40500020980835) {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 3; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 8; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        classes[0] = 10; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_120(float features[]) {
    int classes[5];
    
    if (features[49] <= 26.28499984741211) {
        if (features[42] <= -12.890000343322754) {
            if (features[53] <= -54.170000076293945) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 6; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 11; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 6; 
            classes[4] = 0; 
        }
    } else {
        if (features[58] <= -1.2899999618530273) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 7; 
        } else {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_121(float features[]) {
    int classes[5];
    
    if (features[59] <= 37.450000286102295) {
        if (features[47] <= -20.635000228881836) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 9; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[44] <= -22.989999771118164) {
                if (features[92] <= 20.144999504089355) {
                    classes[0] = 9; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 1; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                if (features[65] <= -45.535000801086426) {
                    classes[0] = 0; 
                    classes[1] = 2; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 9; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_122(float features[]) {
    int classes[5];
    
    if (features[59] <= 48.8799991607666) {
        if (features[50] <= 12.615000486373901) {
            if (features[37] <= 30.850000381469727) {
                classes[0] = 12; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 1; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[47] <= 24.22000026702881) {
                if (features[13] <= 20.09000074863434) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 7; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 1; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_123(float features[]) {
    int classes[5];
    
    if (features[12] <= 39.630001068115234) {
        if (features[78] <= 24.219999313354492) {
            if (features[34] <= 17.585000038146973) {
                if (features[72] <= -55.19499969482422) {
                    classes[0] = 0; 
                    classes[1] = 6; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 7; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 9; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 7; 
        }
    } else {
        if (features[49] <= 28.894999265670776) {
            classes[0] = 10; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 1; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_124(float features[]) {
    int classes[5];
    
    if (features[59] <= 48.8799991607666) {
        if (features[76] <= -68.42499923706055) {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[48] <= 69.26499938964844) {
                if (features[91] <= 20.710000038146973) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 4; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 9; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_125(float features[]) {
    int classes[5];
    
    if (features[61] <= 45.94500160217285) {
        if (features[20] <= -10.349999904632568) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 10; 
            classes[4] = 0; 
        } else {
            if (features[21] <= -6.684999942779541) {
                classes[0] = 0; 
                classes[1] = 10; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[45] <= -47.2900013923645) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 3; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 6; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_126(float features[]) {
    int classes[5];
    
    if (features[59] <= 48.8799991607666) {
        if (features[58] <= 16.6850004196167) {
            if (features[14] <= 32.19499969482422) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[69] <= -12.244998931884766) {
                classes[0] = 0; 
                classes[1] = 11; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 3; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_127(float features[]) {
    int classes[5];
    
    if (features[63] <= 17.165000200271606) {
        if (features[14] <= 28.980000495910645) {
            if (features[55] <= -39.91999912261963) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 4; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 12; 
                classes[4] = 0; 
            }
        } else {
            if (features[30] <= -28.10499906539917) {
                classes[0] = 0; 
                classes[1] = 3; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 11; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_128(float features[]) {
    int classes[5];
    
    if (features[72] <= -49.51499938964844) {
        classes[0] = 0; 
        classes[1] = 12; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[20] <= 35.14499855041504) {
            if (features[51] <= 31.52500057220459) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            } else {
                if (features[61] <= 0.970001220703125) {
                    classes[0] = 0; 
                    classes[1] = 1; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 6; 
                }
            }
        } else {
            if (features[41] <= -14.010000348091125) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 8; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 6; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_129(float features[]) {
    int classes[5];
    
    if (features[47] <= -28.005000114440918) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 11; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[31] <= -46.834999084472656) {
            classes[0] = 0; 
            classes[1] = 8; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[40] <= 47.40999984741211) {
                if (features[65] <= -48.19999980926514) {
                    classes[0] = 6; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 9; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_130(float features[]) {
    int classes[5];
    
    if (features[84] <= 34.55000019073486) {
        if (features[63] <= -40.90499973297119) {
            if (features[49] <= 26.334999799728394) {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[32] <= -34.15000104904175) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 6; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 6; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_131(float features[]) {
    int classes[5];
    
    if (features[51] <= -21.045000076293945) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[15] <= 11.019999980926514) {
            if (features[24] <= 14.864999771118164) {
                if (features[66] <= -22.839999675750732) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 3; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 8; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 10; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_132(float features[]) {
    int classes[5];
    
    if (features[55] <= -45.53499984741211) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 9; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[26] <= 29.639999389648438) {
            if (features[80] <= -51.38999938964844) {
                classes[0] = 0; 
                classes[1] = 10; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[40] <= 53.130001068115234) {
                    if (features[56] <= 21.6200008392334) {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 5; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 1; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 6; 
                }
            }
        } else {
            classes[0] = 9; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_133(float features[]) {
    int classes[5];
    
    if (features[16] <= 71.59000015258789) {
        if (features[82] <= -56.78999900817871) {
            classes[0] = 0; 
            classes[1] = 10; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[51] <= -13.609999656677246) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 7; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[84] <= 16.850000143051147) {
                    if (features[12] <= 3.3550000190734863) {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 3; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 7; 
                }
            }
        }
    } else {
        classes[0] = 12; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_134(float features[]) {
    int classes[5];
    
    if (features[88] <= 26.204999923706055) {
        if (features[71] <= -34.070000886917114) {
            if (features[63] <= -31.394999504089355) {
                if (features[80] <= -41.834999084472656) {
                    classes[0] = 0; 
                    classes[1] = 3; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 4; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 13; 
                classes[4] = 0; 
            }
        } else {
            if (features[25] <= 9.530000194907188) {
                classes[0] = 1; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 11; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 8; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_135(float features[]) {
    int classes[5];
    
    if (features[27] <= 22.505000114440918) {
        if (features[74] <= -28.19499969482422) {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[25] <= -49.58500099182129) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            } else {
                if (features[62] <= -32.20000076293945) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 6; 
                } else {
                    classes[0] = 7; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_136(float features[]) {
    int classes[5];
    
    if (features[40] <= 47.445000648498535) {
        if (features[65] <= -62.505001068115234) {
            if (features[47] <= 31.794999599456787) {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            if (features[71] <= -38.254998445510864) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 4; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 11; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_137(float features[]) {
    int classes[5];
    
    if (features[37] <= 54.88999938964844) {
        if (features[55] <= 74.21000099182129) {
            if (features[31] <= -29.52500057220459) {
                classes[0] = 0; 
                classes[1] = 9; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[91] <= 16.770000457763672) {
                    if (features[79] <= 81.59499931335449) {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 6; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                } else {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 11; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_138(float features[]) {
    int classes[5];
    
    if (features[50] <= -1.8299999237060547) {
        if (features[87] <= 48.795000076293945) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 3; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 13; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[47] <= -27.1850004196167) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 8; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[42] <= 54.87500190734863) {
                if (features[53] <= 10.114999771118164) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 5; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 4; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 7; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_139(float features[]) {
    int classes[5];
    
    if (features[53] <= 9.854999732226133) {
        if (features[47] <= -22.019999980926514) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 6; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[58] <= -45.53499984741211) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            } else {
                classes[0] = 12; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[13] <= 10.255000233650208) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 6; 
        } else {
            classes[0] = 0; 
            classes[1] = 8; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_140(float features[]) {
    int classes[5];
    
    if (features[84] <= 34.55000019073486) {
        if (features[62] <= -17.18499994277954) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 12; 
            classes[4] = 0; 
        } else {
            if (features[50] <= 43.36000061035156) {
                if (features[63] <= -75.18999862670898) {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    if (features[20] <= 77.16500091552734) {
                        classes[0] = 0; 
                        classes[1] = 0; 
                        classes[2] = 4; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    } else {
                        classes[0] = 1; 
                        classes[1] = 0; 
                        classes[2] = 0; 
                        classes[3] = 0; 
                        classes[4] = 0; 
                    }
                }
            } else {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 10; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_141(float features[]) {
    int classes[5];
    
    if (features[27] <= 19.30999994277954) {
        if (features[86] <= 24.760000228881836) {
            if (features[35] <= 24.660000801086426) {
                if (features[43] <= 25.174999237060547) {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 4; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 7; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 13; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 11; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_142(float features[]) {
    int classes[5];
    
    if (features[13] <= 15.079999923706055) {
        if (features[61] <= 19.704999446868896) {
            if (features[40] <= -39.69499969482422) {
                if (features[14] <= 38.14999961853027) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 5; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 8; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 11; 
        }
    } else {
        classes[0] = 0; 
        classes[1] = 10; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 0; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_143(float features[]) {
    int classes[5];
    
    if (features[61] <= 20.62999963760376) {
        if (features[34] <= -52.364999771118164) {
            if (features[31] <= -20.809998989105225) {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[33] <= 26.920000076293945) {
                    classes[0] = 4; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 5; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        } else {
            if (features[26] <= 49.3050012588501) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 14; 
                classes[4] = 0; 
            } else {
                classes[0] = 1; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 8; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_144(float features[]) {
    int classes[5];
    
    if (features[51] <= -19.954999923706055) {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 10; 
        classes[3] = 0; 
        classes[4] = 0; 
    } else {
        if (features[35] <= 22.40000057220459) {
            if (features[68] <= -14.684999942779541) {
                if (features[56] <= 16.45000061392784) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 7; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 5; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            } else {
                classes[0] = 8; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 10; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_145(float features[]) {
    int classes[5];
    
    if (features[27] <= 22.505000114440918) {
        if (features[46] <= 3.9950003623962402) {
            if (features[87] <= 96.90999984741211) {
                classes[0] = 11; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 1; 
                classes[4] = 0; 
            }
        } else {
            if (features[83] <= 32.40000057220459) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 6; 
            } else {
                if (features[67] <= -62.60499954223633) {
                    classes[0] = 0; 
                    classes[1] = 4; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 5; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        if (features[85] <= 66.58499908447266) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 12; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 1; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_146(float features[]) {
    int classes[5];
    
    if (features[20] <= 3.5800002813339233) {
        if (features[26] <= -41.60000038146973) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 11; 
            classes[4] = 0; 
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        }
    } else {
        if (features[26] <= -2.350000023841858) {
            classes[0] = 0; 
            classes[1] = 9; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            if (features[51] <= -43.83000087738037) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 5; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 7; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_147(float features[]) {
    int classes[5];
    
    if (features[23] <= -17.56500005722046) {
        if (features[39] <= -7.869999885559082) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 8; 
        } else {
            if (features[55] <= 4.374999761581421) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 4; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 7; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        }
    } else {
        if (features[25] <= 25.27999973297119) {
            if (features[69] <= -45.119998931884766) {
                classes[0] = 13; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 2; 
                classes[3] = 0; 
                classes[4] = 0; 
            }
        } else {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 6; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_148(float features[]) {
    int classes[5];
    
    if (features[82] <= 41.76499938964844) {
        if (features[20] <= -10.349999904632568) {
            classes[0] = 0; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 12; 
            classes[4] = 0; 
        } else {
            if (features[23] <= -15.785000324249268) {
                classes[0] = 0; 
                classes[1] = 8; 
                classes[2] = 0; 
                classes[3] = 0; 
                classes[4] = 0; 
            } else {
                if (features[85] <= 65.44999885559082) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 6; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 3; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                }
            }
        }
    } else {
        classes[0] = 0; 
        classes[1] = 0; 
        classes[2] = 0; 
        classes[3] = 0; 
        classes[4] = 11; 
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict_149(float features[]) {
    int classes[5];
    
    if (features[20] <= 67.06500053405762) {
        if (features[13] <= 17.31999969482422) {
            if (features[24] <= -46.775001525878906) {
                classes[0] = 0; 
                classes[1] = 0; 
                classes[2] = 0; 
                classes[3] = 11; 
                classes[4] = 0; 
            } else {
                if (features[78] <= 20.12999963760376) {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 4; 
                    classes[3] = 0; 
                    classes[4] = 0; 
                } else {
                    classes[0] = 0; 
                    classes[1] = 0; 
                    classes[2] = 0; 
                    classes[3] = 0; 
                    classes[4] = 5; 
                }
            }
        } else {
            classes[0] = 0; 
            classes[1] = 8; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    } else {
        if (features[82] <= -38.954999923706055) {
            classes[0] = 0; 
            classes[1] = 1; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        } else {
            classes[0] = 11; 
            classes[1] = 0; 
            classes[2] = 0; 
            classes[3] = 0; 
            classes[4] = 0; 
        }
    }
    int class_idx = 0;
    int class_val = classes[0];
    int i;
    for (i = 1; i < 5; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}

int predict (float features[]) {
    int n_classes = 5;
    int classes[n_classes];
    int i;
    for (i = 0; i < n_classes; i++) {
        classes[i] = 0;
    }

    classes[predict_0(features)]++;
    classes[predict_1(features)]++;
    classes[predict_2(features)]++;
    classes[predict_3(features)]++;
    classes[predict_4(features)]++;
    classes[predict_5(features)]++;
    classes[predict_6(features)]++;
    classes[predict_7(features)]++;
    classes[predict_8(features)]++;
    classes[predict_9(features)]++;
    classes[predict_10(features)]++;
    classes[predict_11(features)]++;
    classes[predict_12(features)]++;
    classes[predict_13(features)]++;
    classes[predict_14(features)]++;
    classes[predict_15(features)]++;
    classes[predict_16(features)]++;
    classes[predict_17(features)]++;
    classes[predict_18(features)]++;
    classes[predict_19(features)]++;
    classes[predict_20(features)]++;
    classes[predict_21(features)]++;
    classes[predict_22(features)]++;
    classes[predict_23(features)]++;
    classes[predict_24(features)]++;
    classes[predict_25(features)]++;
    classes[predict_26(features)]++;
    classes[predict_27(features)]++;
    classes[predict_28(features)]++;
    classes[predict_29(features)]++;
    classes[predict_30(features)]++;
    classes[predict_31(features)]++;
    classes[predict_32(features)]++;
    classes[predict_33(features)]++;
    classes[predict_34(features)]++;
    classes[predict_35(features)]++;
    classes[predict_36(features)]++;
    classes[predict_37(features)]++;
    classes[predict_38(features)]++;
    classes[predict_39(features)]++;
    classes[predict_40(features)]++;
    classes[predict_41(features)]++;
    classes[predict_42(features)]++;
    classes[predict_43(features)]++;
    classes[predict_44(features)]++;
    classes[predict_45(features)]++;
    classes[predict_46(features)]++;
    classes[predict_47(features)]++;
    classes[predict_48(features)]++;
    classes[predict_49(features)]++;
    classes[predict_50(features)]++;
    classes[predict_51(features)]++;
    classes[predict_52(features)]++;
    classes[predict_53(features)]++;
    classes[predict_54(features)]++;
    classes[predict_55(features)]++;
    classes[predict_56(features)]++;
    classes[predict_57(features)]++;
    classes[predict_58(features)]++;
    classes[predict_59(features)]++;
    classes[predict_60(features)]++;
    classes[predict_61(features)]++;
    classes[predict_62(features)]++;
    classes[predict_63(features)]++;
    classes[predict_64(features)]++;
    classes[predict_65(features)]++;
    classes[predict_66(features)]++;
    classes[predict_67(features)]++;
    classes[predict_68(features)]++;
    classes[predict_69(features)]++;
    classes[predict_70(features)]++;
    classes[predict_71(features)]++;
    classes[predict_72(features)]++;
    classes[predict_73(features)]++;
    classes[predict_74(features)]++;
    classes[predict_75(features)]++;
    classes[predict_76(features)]++;
    classes[predict_77(features)]++;
    classes[predict_78(features)]++;
    classes[predict_79(features)]++;
    classes[predict_80(features)]++;
    classes[predict_81(features)]++;
    classes[predict_82(features)]++;
    classes[predict_83(features)]++;
    classes[predict_84(features)]++;
    classes[predict_85(features)]++;
    classes[predict_86(features)]++;
    classes[predict_87(features)]++;
    classes[predict_88(features)]++;
    classes[predict_89(features)]++;
    classes[predict_90(features)]++;
    classes[predict_91(features)]++;
    classes[predict_92(features)]++;
    classes[predict_93(features)]++;
    classes[predict_94(features)]++;
    classes[predict_95(features)]++;
    classes[predict_96(features)]++;
    classes[predict_97(features)]++;
    classes[predict_98(features)]++;
    classes[predict_99(features)]++;
    classes[predict_100(features)]++;
    classes[predict_101(features)]++;
    classes[predict_102(features)]++;
    classes[predict_103(features)]++;
    classes[predict_104(features)]++;
    classes[predict_105(features)]++;
    classes[predict_106(features)]++;
    classes[predict_107(features)]++;
    classes[predict_108(features)]++;
    classes[predict_109(features)]++;
    classes[predict_110(features)]++;
    classes[predict_111(features)]++;
    classes[predict_112(features)]++;
    classes[predict_113(features)]++;
    classes[predict_114(features)]++;
    classes[predict_115(features)]++;
    classes[predict_116(features)]++;
    classes[predict_117(features)]++;
    classes[predict_118(features)]++;
    classes[predict_119(features)]++;
    classes[predict_120(features)]++;
    classes[predict_121(features)]++;
    classes[predict_122(features)]++;
    classes[predict_123(features)]++;
    classes[predict_124(features)]++;
    classes[predict_125(features)]++;
    classes[predict_126(features)]++;
    classes[predict_127(features)]++;
    classes[predict_128(features)]++;
    classes[predict_129(features)]++;
    classes[predict_130(features)]++;
    classes[predict_131(features)]++;
    classes[predict_132(features)]++;
    classes[predict_133(features)]++;
    classes[predict_134(features)]++;
    classes[predict_135(features)]++;
    classes[predict_136(features)]++;
    classes[predict_137(features)]++;
    classes[predict_138(features)]++;
    classes[predict_139(features)]++;
    classes[predict_140(features)]++;
    classes[predict_141(features)]++;
    classes[predict_142(features)]++;
    classes[predict_143(features)]++;
    classes[predict_144(features)]++;
    classes[predict_145(features)]++;
    classes[predict_146(features)]++;
    classes[predict_147(features)]++;
    classes[predict_148(features)]++;
    classes[predict_149(features)]++;

    int class_idx = 0;
    int class_val = classes[0];
    for (i = 1; i < n_classes; i++) {
        if (classes[i] > class_val) {
            class_idx = i;
            class_val = classes[i];
        }
    }
    return class_idx;
}
