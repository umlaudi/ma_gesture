// preprocessing Library
#include <stdint.h>
#include <stdio.h>
#include <math.h>

// Offset Correction of Measured Data
void OffsetCorr(float *data, int len){
	float fOffset = (data[0] + data[1] + data[2])/3;
	for(int i = 0; i<len; i++){
		data[i] = data[i] - fOffset;
	}
}


// Moving Average Filter
void MovingAverage(float * src, float * dst, int len, int mask){
	float sum = 0;
	int maskFirstHalf = (int)(((mask-1)/2));
	for(int i = 0; i < len; i++){
		if(i <= len-mask){
			for(int j=0; j<mask;j++){
				sum += src[i+j];
			}
			dst[i] = (sum/mask);
			sum = 0;
		}
	}
	for(int i=0; i<maskFirstHalf;i++){
		src[i]=0;
		src[(len-1)-i]=0;
	}
	for(int i=0; i<=(len-mask); i++){
		src[i+maskFirstHalf] = dst[i];
	}
}

// Interpolation
void Interpolate(float *src, float *dst, int len, int newLen){
	
	float fOldLen = (float)(len-1);
	float fNewLen = (float)(newLen - 1);
	float fDeltaLen = fOldLen / fNewLen;
	int idx = 0;
	float f = 0.0;
	
	for(int i = 0; i<newLen; i++){
		idx = (int)(i*fDeltaLen);
		f = (float)((i*1.0)*fDeltaLen) - (float)idx;
		if(f > 0.0005){
			dst[i] = src[idx]+ f*(src[idx+1]-src[idx]);
		}else{
			dst[i] = src[idx];
		}
	}
}

//Normalize
void Normalize(float * data, int len, int newMaxVal, float fMin, float fMax){
	float maxv=0, minv=data[0];
	for(int i = 0; i<len; i++){
		if(data[i] > maxv) maxv=data[i];
		if(data[i] < minv) minv=data[i];
	}
	if (fabs(minv) > maxv){
		maxv = fabs(minv);
	}
	for(int i = 0; i < len; i++){
		data[i] = (data[i] / maxv) * newMaxVal;
	}	
}

//Calculate Pitches of data
void CalcPitch(float * src, int len, float *pData, int mask){
	int maskFirstHalf = (int)((mask-1)/2);
	float avgX = 0.0, avgY=0.0, za=0.0, ne=0.0;
	for(int i = 0; i<len; i++){
		if(i <= len - mask){
			for(int j = 0; j<mask;j++){
				avgX += i+j;
				avgY += src[i+j];
			}
			avgY = avgY / mask;
			avgX = avgX / mask;
			for(int j=0; j<mask;j++){
				za += ((i+j)-avgX)*(src[i+j]-avgY);
				ne += ((i+j)-avgX) * ((i+j)-avgX);
			}
			pData[i+maskFirstHalf] = za/ne;
			za=0.0; ne=0.0; avgX=0.0; avgY=0.0;
		}
	}
	for(int i=0; i<maskFirstHalf;i++){
		pData[i]=0;
		pData[(len-1)-i]=0;
	}
}
	
// classify pitchdata
void ClassifyPitch(float* data, int len, float* pitches, int* dst){
	float maxv = 0, minv=data[0];
	float maxp = 0, minp=pitches[0];
	float hystvp, hystvn, hystpp, hystpn;
	int LastClass = 0;
	for(int i = 0; i<len; i++){
		if(data[i] > maxv) maxv=data[i];
		if(data[i] < minv) minv=data[i];
		if(pitches[i] > maxp) maxp=pitches[i];
		if(pitches[i] < minp) minp=pitches[i];
	}
	hystvp = maxv/3;hystpp = maxp/2;
	hystvn = minv/3;hystpn = minp/2;
	for(int i = 0; i<len;i++){
		if (data[i] >= hystvp){
			dst[i] = 1;
		}else if(data[i] <= hystvn){
			dst[i] = -1;
		}else{
			if(LastClass == -1 && pitches[i] > hystpp){
				dst[i] = -1;
			}else if(LastClass == 1 && pitches[i] < hystpn){
				dst[i] = 1;
			}else{
				dst[i] = 0;
			}
		}
	}
}

// Classiffy direction data
int ClassifyDir(int x, int z){
    if (x == 0){
        if (z == 0){
            return 0;
		}else if(z == 1){
            return 5;
        }else if (z == -1){
            return 1;
		}
    }else if (x == 1){
        if (z == 0){
            return 3;
        }else if (z == 1){
            return 4;
        }else if (z == -1){
            return 2;
		}
    }else if (x == -1){
        if (z == 0){
            return 7;
        }else if (z == 1){
            return 6;
        }else if (z == -1){
            return 8;
		}
	}
	return 0;
}
