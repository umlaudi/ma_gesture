#define MAX_STATES 5
#define NUM_OBS 50
#define NUM_EMS 9
#include "Probabilities.h"

// Forward Algorithm
float Forward(float *Pi, float *b, float *a, int nS, int *o){
	static float f[NUM_OBS][MAX_STATES];
	int nO = NUM_OBS;
	float sum=0.0;
	float P=0;
	
	for(int i=0;i<nS;i++){
		f[0][i] = Pi[i] * b[o[0]];
	}
	for(int t=1;t<nO;t++){
		for(int i=0;i<nS;i++){
			sum=0.0;
			for(int j=0;j<nS;j++){
				sum += f[t-1][j]*a[(j*nS)+i];
			}
			sum *= b[(i*NUM_EMS) + o[t]];
            f[t][i] = sum;
		}
	}
	for(int j=0; j<nS;j++){
        P += f[nO-1][j];
	}
	return P;
}

int CalcForward(int * obs){
	float P = 0.0;
	float MaxP = 0.0;
	int idx = 0;
	for(int i=0;i<5;i++){
		switch(i){
		case 0:
			P = Forward(pi_1, b_1, a_1, nS_1, obs);
			break;
		case 1:
			P = Forward(pi_3, b_3, a_3, nS_3, obs);
			break;
		case 2:
			P = Forward(pi_kringel, b_kringel, a_kringel, nS_kringel, obs);
			break;
		case 3:
			P = Forward(pi_4, b_4, a_4, nS_4, obs);
			break;
		case 4:
			P = Forward(pi_6, b_6, a_6, nS_6, obs);
			break;
		};
		if(P > MaxP){
			MaxP = P;
			idx = i;
		}
	}
	return idx;
}

/*// Forward Algorithm
float Forward(float *Pi[], float *b[], float *a[], int *o){
	static float f[NUM_OBS][MAX_STATES];
	int nS = sizeof(a);
	int nO = sizeof(o);
	float sum=0.0;
	float P=0;
	
	for(int i=0;i<nS;i++){
		f[0][i] = Pi[0][i] * b[0][o[0]];
	}
	for(int t=1;t<nO;t++){
		for(int i=0;i<nS;i++){
			sum=0.0;
			for(int j=0;j<nS;j++){
				sum += f[t-1][j]*a[j][i];
			}
			sum *= b[i][o[t]];
            f[t][i] = sum;
		}
	}
	for(int j=0; j<nS;j++){
        P += f[nO-1][j];
	}
	return P;
}*/
